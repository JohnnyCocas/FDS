package data.missions.fds_familiar_grounds;

import com.fs.starfarer.api.fleet.FleetGoal;
import com.fs.starfarer.api.fleet.FleetMemberType;
import com.fs.starfarer.api.mission.FleetSide;
import com.fs.starfarer.api.mission.MissionDefinitionAPI;
import com.fs.starfarer.api.mission.MissionDefinitionPlugin;

/**
 *
 * @author MadHSSniper a.k.a. Johnny Cocks a.k.a. joaonunes
 */
public class MissionDefinition implements MissionDefinitionPlugin {

    @Override
    public void defineMission(MissionDefinitionAPI api) {
        //Setting up fleets
        //One attacking and other defending
        api.initFleet(FleetSide.PLAYER, "FDS", FleetGoal.ATTACK, false);
        api.initFleet(FleetSide.ENEMY, "HSS", FleetGoal.ATTACK, true);

        // Set a small blurb for each fleet that shows up on the mission detail and
        // mission results screens to identify each side.
        api.setFleetTagline(FleetSide.PLAYER, "Syndicate Infiltration Squad");
        api.setFleetTagline(FleetSide.ENEMY, "Hegemony Fast Response Fleet");

        // These show up as items in the bulleted list under 
        // "Tactical Objectives" on the mission detail screen
        api.addBriefingItem("Leave no survivors!");

        // Set up the player's fleet. Use "variant" names
        api.addToFleet(FleetSide.PLAYER, "fds_retaliation_assault", FleetMemberType.SHIP, "Blue Leader", true);
        api.addToFleet(FleetSide.PLAYER, "fds_despair_standard", FleetMemberType.SHIP, "Blue I", false);
        api.addToFleet(FleetSide.PLAYER, "fds_rancour_support", FleetMemberType.SHIP, "Blue II", false);
        api.addToFleet(FleetSide.PLAYER, "fds_disturbance_standard", FleetMemberType.SHIP, "Red Leader", true);
        api.addToFleet(FleetSide.PLAYER, "fds_grief_mk_ii_assault", FleetMemberType.SHIP, "Red I", false);
        api.addToFleet(FleetSide.PLAYER, "fds_grief_mk_ii_strike", FleetMemberType.SHIP, "Red II", false);
        api.addToFleet(FleetSide.PLAYER, "fds_sorrow_standard", FleetMemberType.SHIP, "Green Leader", true);
        api.addToFleet(FleetSide.PLAYER, "fds_melancholy_assault", FleetMemberType.SHIP, "Green I", false);
        api.addToFleet(FleetSide.PLAYER, "fds_melancholy_assault", FleetMemberType.SHIP, "Green II", false);
        api.addToFleet(FleetSide.PLAYER, "fds_agony_assault", FleetMemberType.SHIP, "Green III", false);
        api.addToFleet(FleetSide.PLAYER, "fds_agony_assault", FleetMemberType.SHIP, "Green IV", false);
        api.addToFleet(FleetSide.PLAYER, "fds_agony_mk_ii_standard", FleetMemberType.SHIP, "Green V", false);


        api.addToFleet(FleetSide.ENEMY, "heron_Attack", FleetMemberType.SHIP, "Valiant", true);
        api.addToFleet(FleetSide.ENEMY, "falcon_xiv_Elite", FleetMemberType.SHIP, "Hunter", false);
        api.addToFleet(FleetSide.ENEMY, "hammerhead_Balanced", FleetMemberType.SHIP, "Hammer", false);
        api.addToFleet(FleetSide.ENEMY, "hammerhead_Support", FleetMemberType.SHIP, "Anvil", false);
        api.addToFleet(FleetSide.ENEMY, "sunder_Assault", FleetMemberType.SHIP, "Piercer", false);
        api.addToFleet(FleetSide.ENEMY, "enforcer_Balanced", FleetMemberType.SHIP, "Executioner", false);
        api.addToFleet(FleetSide.ENEMY, "wolf_hegemony_Assault", FleetMemberType.SHIP, "Pack leader", true);
        api.addToFleet(FleetSide.ENEMY, "lasher_Strike", FleetMemberType.SHIP, "Slasher", false);
        api.addToFleet(FleetSide.ENEMY, "vigilance_Strike", FleetMemberType.SHIP, "Guardian", false);
        api.addToFleet(FleetSide.ENEMY, "vigilance_Strike", FleetMemberType.SHIP, "Watchman", false);

        // Set up the map.
        // 12000x8000 is actually somewhat small, making for a faster-paced mission.
        float width = 10000f;
        float height = 15000f;
        api.initMap((float) -width / 2f, (float) width / 2f, (float) -height / 2f, (float) height / 2f);
        api.addAsteroidField(0, 0, 0, width, 10, 100, 1000);

        float minX = -width / 2;
        float minY = -height / 2;

        // All the addXXX methods take a pair of coordinates followed by data for
        // whatever object is being added.
        // Add two big nebula clouds
        api.addNebula(width * 0.5f, height * 0.5f, 20000);

        // Add objectives. These can be captured by each side
        // and provide stat bonuses and extra command points to
        // bring in reinforcements.
        // Reinforcements only matter for large fleets - in this
        // case, assuming a 100 command point battle size,
        // both fleets will be able to deploy fully right away.
        /*api.addObjective(minX + width * 0.75f, minY + height * 0.25f, "sensor_array");
        api.addObjective(minX + width * 0.75f, minY + height * 0.75f, "nav_buoy");
        api.addObjective(minX + width * 0.5f, minY + height * 0.6f, "comm_relay");
        api.addObjective(minX + width * 0.25f, minY + height * 0.75f, "sensor_array");
        api.addObjective(minX + width * 0.25f, minY + height * 0.25f, "nav_buoy");
         */
    }
}
