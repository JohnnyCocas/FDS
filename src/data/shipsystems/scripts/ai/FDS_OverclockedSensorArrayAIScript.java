package data.shipsystems.scripts.ai;

import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.util.IntervalUtil;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lwjgl.util.vector.Vector2f;

import java.util.List;

/**
 *
 * @author MadHSSniper a.k.a. Johnny Cocks a.k.a. joaonunes
 */
public class FDS_OverclockedSensorArrayAIScript implements ShipSystemAIScript {
    private ShipAPI ship;
    private ShipwideAIFlags flags;
    private CombatEngineAPI engine;
    private ShipSystemAPI system;
    private final IntervalUtil tracker = new IntervalUtil(0.4f, 0.6f);

    public FDS_OverclockedSensorArrayAIScript() {}

    public void init(ShipAPI ship, ShipSystemAPI system, ShipwideAIFlags flags, CombatEngineAPI engine) {
        this.ship = ship;
        this.flags = flags;
        this.engine = engine;
        this.system = system;
    }

    public void advance(float amount, Vector2f missileDangerDir, Vector2f collisionDangerDir, ShipAPI target) {
        if (this.engine != null) {
            if (!this.engine.isPaused()) {
                this.tracker.advance(amount);
                if (this.tracker.intervalElapsed()) {
                    List<ShipAPI> allies = AIUtils.getAlliesOnMap(ship);
                    List<ShipAPI> enemies = AIUtils.getEnemiesOnMap(ship);

                    if (!this.system.isActive()) {
                        boolean activeFleet = false;
                        for (ShipAPI s: allies) {
                            if (s.getHullSpec().getHullId() == ship.getHullSpec().getHullId() && s.getSystem().isOn()) {
                                activeFleet = true;
                            }
                        }
                        if (enemies.size() > 0 && !activeFleet) {
                            this.ship.useSystem();
                        }
                    }
                }
            }
        }
    }
}
