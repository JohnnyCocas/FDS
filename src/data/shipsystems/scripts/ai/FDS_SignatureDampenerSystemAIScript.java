package data.shipsystems.scripts.ai;

import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.util.IntervalUtil;
import data.shipsystems.scripts.FDS_SignatureDampenerSystemScript;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lwjgl.util.vector.Vector2f;

import java.util.List;

/**
 *
 * @author MadHSSniper a.k.a. Johnny Cocks a.k.a. joaonunes
 */
public class FDS_SignatureDampenerSystemAIScript implements ShipSystemAIScript {
    private float maxRange;
    private ShipAPI ship;
    private ShipwideAIFlags flags;
    private CombatEngineAPI engine;
    private ShipSystemAPI system;
    private final IntervalUtil tracker = new IntervalUtil(0.4f, 0.6f);

    public FDS_SignatureDampenerSystemAIScript() {}

    public void init(ShipAPI ship, ShipSystemAPI system, ShipwideAIFlags flags, CombatEngineAPI engine) {
        this.ship = ship;
        this.flags = flags;
        this.engine = engine;
        this.system = system;
        this.maxRange = FDS_SignatureDampenerSystemScript.MAX_RANGE;
    }

    public void advance(float amount, Vector2f missileDangerDir, Vector2f collisionDangerDir, ShipAPI target) {
        if (this.engine != null) {
            if (!this.engine.isPaused()) {
                this.tracker.advance(amount);
                if (this.tracker.intervalElapsed()) {
                    FluxTrackerAPI flux = this.ship.getFluxTracker();
                    List<ShipAPI> enemies = AIUtils.getNearbyEnemies(ship, maxRange);
                    List<ShipAPI> closeEnemies = AIUtils.getNearbyEnemies(ship, (float)(maxRange*0.9));
                    List<ShipAPI> allies = AIUtils.getAlliesOnMap(ship); //.getNearbyAllies(ship, maxRange);

                    if (this.system.isActive()) {
                        if ((flux.getCurrFlux() >= flux.getMaxFlux()*0.9 && this.flags.hasFlag(ShipwideAIFlags.AIFlags.HAS_INCOMING_DAMAGE))
                                || enemies.size() == 0
                                || (flux.getHardFlux() >= flux.getMaxFlux()*0.9)){
                            this.ship.useSystem();
                        }
                    } else if (flux.getCurrFlux() <= flux.getMaxFlux()*0.2) {
                        boolean activeFleet = false;
                        for (ShipAPI s: allies) {
                            if (s.getHullSpec().getHullId() == ship.getHullSpec().getHullId() && s.getSystem().isOn() &&
                                    MathUtils.getDistance(ship.getLocation(), s.getLocation()) <= maxRange * 1.75) {
                                activeFleet = true;
                            }
                        }
                        if (closeEnemies.size() > 0 && !activeFleet) {
                            this.ship.useSystem();
                        }
                    }
                }
            }
        }
    }
}
