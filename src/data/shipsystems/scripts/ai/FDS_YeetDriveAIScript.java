package data.shipsystems.scripts.ai;

import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.util.IntervalUtil;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lwjgl.util.vector.Vector2f;

import static data.scripts.campaign.ids.FDS_IDs.DEBUFF_SIGNATURE_DAMPENER;

/**
 *
 * @author MadHSSniper a.k.a. Johnny Cocks a.k.a. joaonunes
 */
public class FDS_YeetDriveAIScript implements ShipSystemAIScript {
    private static final float minRange = 1000f;
    private ShipAPI ship;
    private ShipwideAIFlags flags;
    private CombatEngineAPI engine;
    private ShipSystemAPI system;
    private final IntervalUtil tracker = new IntervalUtil(0.1f, 0.2f);
    private final IntervalUtil timer = new IntervalUtil(2f, 2f);
    private boolean trigger = false;

    public FDS_YeetDriveAIScript() {}

    public void init(ShipAPI ship, ShipSystemAPI system, ShipwideAIFlags flags, CombatEngineAPI engine) {
        this.ship = ship;
        this.flags = flags;
        this.engine = engine;
        this.system = system;
    }

    public void advance(float amount, Vector2f missileDangerDir, Vector2f collisionDangerDir, ShipAPI target) {
        if (this.engine != null) {
            if (this.engine.isPaused()) {
                return;
            }

            if (trigger) {
                this.timer.advance(engine.getElapsedInLastFrame());
                if (this.timer.intervalElapsed()) {
                    this.ship.useSystem();
                    trigger = false;
                }
            }

            this.tracker.advance(engine.getElapsedInLastFrame());
            if (this.tracker.intervalElapsed()) {

                if (system.isOutOfAmmo() || (system.getCooldownRemaining() > 0 && !system.isOn())) {
                    return;
                }

                // Prevent activation when under the effect of the Gravity Well Generator
                if (ship.getMutableStats().getMaxSpeed().getMultStatMod(DEBUFF_SIGNATURE_DAMPENER) != null) {
                    if (this.system.isActive()) {
                        this.ship.useSystem();
                    }
                    return;
                }

                ShipAPI trgt = target;
                float angle;
                float distance;

                // Shut down if near the map's border
                if (this.system.isActive() && (this.flags.hasFlag(ShipwideAIFlags.AIFlags.AVOIDING_BORDER))) {
                    this.ship.useSystem();
                }

                if (trgt == null) {
                    return;
//                    trgt = ship.getShipTarget();
//                    trgt = AIUtils.getNearestEnemy(ship);
                }
                angle = Math.abs(MathUtils.getShortestRotation(ship.getFacing(), VectorUtils.getAngle(ship.getLocation(), trgt.getLocation())));
                distance = MathUtils.getDistance(ship, trgt);

                if (this.system.isActive()) {
                    if ((!this.flags.hasFlag(ShipwideAIFlags.AIFlags.PURSUING) && (angle > 60 || distance < 600)) ||
                          this.flags.hasFlag(ShipwideAIFlags.AIFlags.PURSUING) && (angle > 80 || distance < 400)) { // More aggressive when Pursuing
                        trigger = true;
                    }
                } else if (trgt.isAlive() && !trgt.isAlly()) {
//                    this.ship.getAIFlags().setFlag(ShipwideAIFlags.AIFlags.MANEUVER_TARGET, ShipwideAIFlags.FLAG_DURATION);
                    if (distance >= minRange) {
                        if (angle <= 5 && distance < minRange * 1.5f) {
                            this.ship.useSystem();
                        } else if (angle <= 2 && distance < minRange * 2f) {
                            this.ship.useSystem();
                        } else if (angle <= 1 && distance >= minRange * 2f) {
                            this.ship.useSystem();
                        }
                    }
                } else if (this.flags.hasFlag(ShipwideAIFlags.AIFlags.PURSUING)) {
                    this.ship.useSystem();
                }
            }
        }
    }
}
