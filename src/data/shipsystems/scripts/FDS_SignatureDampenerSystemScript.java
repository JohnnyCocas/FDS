package data.shipsystems.scripts;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;
import com.fs.starfarer.api.plugins.ShipSystemStatsScript;

import java.awt.*;
import java.util.*;
import java.util.List;

import data.scripts.campaign.ids.FDS_IDs;
import data.scripts.plugins.FDS_SpriteRenderManager;
import org.apache.log4j.Level;

import org.dark.shaders.distortion.DistortionShader;
import org.dark.shaders.distortion.RippleDistortion;
import org.dark.shaders.distortion.WaveDistortion;
import org.dark.shaders.light.LightShader;
import org.dark.shaders.light.StandardLight;
import org.dark.shaders.util.ShaderLib;
import org.lazywizard.lazylib.FastTrig;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lazywizard.lazylib.combat.entities.SimpleEntity;
import org.lwjgl.util.vector.Vector2f;

/**
 *
 * @author MadHSSniper a.k.a. Johnny Cocks a.k.a. joaonunes
 */
public class FDS_SignatureDampenerSystemScript extends BaseShipSystemScript {

    // function: (-cos(3*range)+1)/2
    // Range in su, public so the AI script can use this value
    public static final float MAX_RANGE = 2500f;
    // Final speed relative to starting speed
    private static final float TAG = 0.99999f;//Simply to tag affected ships like phased ones;
    private static final float CAPITAL_SPEED = 0.99999f;//0.9f;
    private static final float CRUISER_SPEED = 0.95f;//0.85f;
    private static final float DESTROYER_SPEED = 0.90f;//0.75f;
    private static final float FRIGATE_SPEED = 0.80f;//0.5f;
    private static final float FIGHTER_SPEED = 0.75f;//0.3f;
    private static final float PHASE_COST = 2.0f;//100% increased cost to activate
    private static final float PHASE_UPKEEP_COST = 1.25f;//25% increased cost to maintain
    private static final String SYSTEM_LOOP = "system_dampener_loop";
    private static final Vector2f ZERO = new Vector2f();
    private List<ShipAPI> affectedShips = new ArrayList();

    private WaveDistortion wave;
    private StandardLight light;
    private boolean isActive = false;


    public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
        CombatEngineAPI engine = Global.getCombatEngine();
        // Weaken own shields
        stats.getShieldAbsorptionMult().modifyMult(id, 0.5f);
        stats.getMaxSpeed().modifyMult(id, 0.75f);

        ShipAPI ship = (ShipAPI)stats.getEntity();
        if (ship != null) {
            Map<ShipAPI, RippleDistortion> maps = (Map)Global.getCombatEngine().getCustomData().get(FDS_IDs.DEBUFF_SIGNATURE_DAMPENER);
            Vector2f loc = ship.getLocation();
            RippleDistortion halo;
            if (!maps.containsKey(ship)) {
                halo = new RippleDistortion(loc, ZERO);
                halo.setCurrentFrame(50f);
                DistortionShader.addDistortion(halo);
                maps.put(ship, halo);
            } else {
                halo = maps.get(ship);
            }

            float amount = engine.getElapsedInLastFrame();
            float range = MAX_RANGE * (float)(-Math.cos(3*effectLevel)+1)/2;
            halo.setSize(range);
            halo.setIntensity(10f * effectLevel);
            halo.setLocation(loc);
            float fluxLevel = ship.getFluxLevel();
            if (fluxLevel < 0.5) {
                fluxLevel = 0;
            } else {
                fluxLevel = (fluxLevel - 0.5f) / 0.5f;
            }
            Color color = this.interpolateColor(new Color(0, 0, 255), new Color(255, 0, 0), fluxLevel);
            int count = Math.round(effectLevel * 350f * amount);

            Global.getSoundPlayer().playLoop(SYSTEM_LOOP, ship, 1f, (float)(effectLevel * 0.5), ship.getLocation(), ship.getVelocity());

            FDS_SpriteRenderManager.singleFrameRender(
                    Global.getSettings().getSprite("misc", "fds_signature_dampener_area"),
                    ship.getLocation(),
                    new Vector2f(range*2, range*2),
                    0,
                    color,
                    true
            );

            Vector2f point;
            float radius;
            float angle;
            for(int i = 0; i < count; ++i) {
                point = MathUtils.getRandomPointInCircle(loc, range);
                if (ShaderLib.isOnScreen(point, 200)) {
                float dist = MathUtils.getDistance(loc, point);
                radius = MathUtils.getRandomNumberInRange(150f, 250f) * effectLevel * (float)((dist+((range-dist)*0.2))/range);
                angle = VectorUtils.getAngle(point, loc);

                Vector2f vel = MathUtils.getPointOnCircumference(null, radius, angle);
                float duration = MathUtils.getRandomNumberInRange(2f, 5f);
                float size = MathUtils.getRandomNumberInRange(10f, 20f);
                engine.addSmoothParticle(point, vel, size, 1f, duration, color);

//                engine.addHitParticle(point, point, size, 1f, 3f, Color.BLUE);
//                Vector2f spark = MathUtils.getRandomPointInCircle(loc, range);
//                Vector2f spark2 = spark;
//                spark2.set(spark.getX()+1, spark.getY()+1);
//                engine.spawnEmpArc(ship, spark, null, new SimpleEntity(spark2), DamageType.OTHER, 0.0F, 0.0F, 10000.0F, null, (float)Math.random() * 40.0F * effectLevel + 20.0F, Color.BLUE, Color.WHITE);
                }
            }

            float possibility = effectLevel * 30.0F * amount;
            if ((float)Math.random() < possibility) {
                Vector2f pointa = MathUtils.getRandomPointOnCircumference(loc, range);
                float pointaAngle = VectorUtils.getAngle(loc, pointa);
                Vector2f pointb = MathUtils.getPointOnCircumference(loc, range, pointaAngle + 10f);
                engine.spawnEmpArc(ship, pointa, null, new SimpleEntity(pointb), DamageType.OTHER, 0.0F, 0.0F, 10000.0F, null, (float)Math.random() * 40.0F * effectLevel + 20.0F, color, Color.WHITE);
            }

            // Debuff all enemies in range
            List<ShipAPI> ships = CombatUtils.getShipsWithinRange(loc, range);
            //id = id + ship.getId();
            Iterator i$ = ships.iterator();

            ShipAPI target;
            while(i$.hasNext()) {
                target = (ShipAPI)i$.next();
                if (!this.affectedShips.contains(target)) {
                    this.affectedShips.add(target);
                }

                if (target != ship) {
                    if (target.isAlive() && !target.isAlly() && target.getOwner() != ship.getOwner()) {
                        this.applyDebuff(target, FDS_IDs.DEBUFF_SIGNATURE_DAMPENER, ship, range);
                    }
                }
            }

            // Remove all ships in range from affectedShips
            // Only the ones that are out of range will remain (I keep forgetting that)
            this.affectedShips.removeAll(ships);
            i$ = this.affectedShips.iterator();

            // Remove debuffs from out of range ships
            while(i$.hasNext()) {
                target = (ShipAPI)i$.next();
                this.unapplyDebuff(target, FDS_IDs.DEBUFF_SIGNATURE_DAMPENER);
            }

            // Reassign in-range ships to affectedShips
            this.affectedShips = ships;
        }
    }

    private void applyDebuff(ShipAPI target, String id, ShipAPI emitter, float range) {
        // Disable zero flux speed bonus
        MutableShipStatsAPI stats = target.getMutableStats();

        if (target.isPhased()) {
            float distance = MathUtils.getDistance(target, emitter);
            // max percentage = 25%, increasing from 0 to max as the target gets closer to the ship
            target.getMutableStats().getTimeMult().modifyPercent(FDS_IDs.DEBUFF_SIGNATURE_DAMPENER, -(((range-distance)/range)*25));
            stats.getMaxSpeed().modifyMult(id, TAG);
            stats.getPhaseCloakActivationCostBonus().modifyMult(id, PHASE_COST);
            stats.getPhaseCloakUpkeepCostBonus().modifyMult(id, PHASE_UPKEEP_COST);
        } else {
            stats.getZeroFluxMinimumFluxLevel().modifyFlat(id, 1f);
            stats.getZeroFluxSpeedBoost().modifyMult(id, 0f);

            if (target.isCapital()) {
                stats.getMaxSpeed().modifyMult(id, CAPITAL_SPEED);
            } else if (target.isCruiser()) {
                stats.getMaxSpeed().modifyMult(id, CRUISER_SPEED);
            } else if (target.isDestroyer()) {
                stats.getMaxSpeed().modifyMult(id, DESTROYER_SPEED);
            } else if (target.isFrigate()) {
                stats.getMaxSpeed().modifyMult(id, FRIGATE_SPEED);
            } else if (target.isFighter() || target.isDrone()) {
                stats.getMaxSpeed().modifyMult(id, FIGHTER_SPEED);
            } else {
                stats.getMaxSpeed().modifyMult(id, CRUISER_SPEED);
            }
        }
    }

    private void unapplyDebuff(ShipAPI target, String id) {
        // Re-enable zero flux speed bonus
        MutableShipStatsAPI stats = target.getMutableStats();
        stats.getZeroFluxMinimumFluxLevel().unmodify(id);
        stats.getZeroFluxSpeedBoost().unmodify(id);
        stats.getMaxSpeed().unmodify(id);
        stats.getTimeMult().unmodify(id);
    }

    public void unapply(MutableShipStatsAPI stats, String id) {
        ShipAPI ship = (ShipAPI)stats.getEntity();
        if (ship != null) {
            // Bring shields back to full strength
            stats.getShieldAbsorptionMult().unmodify(id);
            stats.getMaxSpeed().unmodify(id);

            Map<ShipAPI, RippleDistortion> maps = (Map)Global.getCombatEngine().getCustomData().get(FDS_IDs.DEBUFF_SIGNATURE_DAMPENER);
            if (maps != null) {
                RippleDistortion halo = maps.get(ship);
                if (halo != null) {
                    DistortionShader.removeDistortion(halo);
                    maps.remove(ship);
                }
            }
        }
    }

    private static Color interpolateColor(Color old, Color dest, float progress)
    {
        final float clampedProgress = Math.max(0f, Math.min(1f, progress));
        final float antiProgress = 1f - clampedProgress;
        final float[] ccOld = old.getComponents(null), ccNew = dest.getComponents(null);
        return new Color((ccOld[0] * antiProgress) + (ccNew[0] * clampedProgress),
                (ccOld[1] * antiProgress) + (ccNew[1] * clampedProgress),
                (ccOld[2] * antiProgress) + (ccNew[2] * clampedProgress),
                (ccOld[3] * antiProgress) + (ccNew[3] * clampedProgress));
    }

    @Override
    public StatusData getStatusData(int index, State state, float effectLevel) {
        int range = (int) (MAX_RANGE * effectLevel);

        if (index == 0) {
            return new StatusData("dampening enemy ships within " + range + " su", false);
        }
        if (index == 1) {
            return new StatusData("shields weakened", true);
        }

        return null;
    }
}
