//by Tartiflette, Anti-missile missile AI: precise and able to randomly choose a target between nearby enemy missiles.
//feel free to use it, credit is appreciated but not mandatory
//V2 done
package data.scripts.weapons.ai;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.GuidedMissileAI;
import com.fs.starfarer.api.combat.MissileAIPlugin;
import com.fs.starfarer.api.combat.MissileAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipCommand;
import com.fs.starfarer.api.util.IntervalUtil;
import com.fs.starfarer.api.util.WeightedRandomPicker;
import data.scripts.util.MagicRender;
import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.lazywizard.lazylib.FastTrig;
import org.lwjgl.util.vector.Vector2f;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lazywizard.lazylib.combat.AIUtils;

public class FDS_PDMissileAI implements MissileAIPlugin, GuidedMissileAI {

    private CombatEngineAPI engine;
    private final MissileAPI missile;
    private CombatEntityAPI target;
    private Vector2f lead = new Vector2f();
    private IntervalUtil timer = new IntervalUtil(0.025f,0.075f);
    private boolean targetOnce=false;
    //data
    private final float MAX_SPEED;
    private final float DAMPING = 0.05f;
    private final Color EXPLOSION_COLOR = new Color(255, 0, 0, 255);
    private final Color PARTICLE_COLOR = new Color(240, 200, 50, 255);
    private final int NUM_PARTICLES = 10;
    private static Map< MissileAPI , MissileAPI > ANTIMISSILES = new HashMap<>();

    //Helper variables
    private final static float N = 3f;
    private Vector2f oldDiff;
    private float oldVel = 0f;

    public FDS_PDMissileAI(MissileAPI missile, ShipAPI launchingShip) {
        this.missile = missile;
        MAX_SPEED = missile.getMaxSpeed();
        timer.randomize();
        this.oldDiff = missile.getLocation();
    }

    @Override
    public void advance(float amount) {

        if (engine != Global.getCombatEngine()) {
            this.engine = Global.getCombatEngine();
        }

        if (Global.getCombatEngine().isPaused() || missile.isFading() || missile.isFizzling()){
            return;
        }

        // if there is no target, assign one
        if (target == null || !engine.isEntityInPlay(target)) {
            missile.giveCommand(ShipCommand.ACCELERATE);
            if(!targetOnce){
                //first targeting, get the best possible
                setTarget(findRandomMissileWithinRange(missile));
            } else {
                //dumbly get the nearest missile as target
                target=AIUtils.getNearestEnemyMissile(missile);
                //if the target isn't null, add it to the master list
                if (target!=null){
                    ANTIMISSILES.put(missile, (MissileAPI)target);
                }
            }
            return;
        }

        //if the script get there, the missile is on its way and won't be able to smartly retarget
        targetOnce=true;

        timer.advance(amount);
        if(timer.intervalElapsed()){
            float dist = MathUtils.getDistanceSquared(missile.getLocation(), target.getLocation());

            //proximity fuse
            if (dist<2500){
                proximityFuse();
                return;
            }

            //finding lead point to aim to
            lead = AIUtils.getBestInterceptPoint(
                    missile.getLocation(),
                    MAX_SPEED,
                    target.getLocation(),
                    target.getVelocity()
            );
            /*lead = getAPNPoint(
                    missile.getLocation(),
                    missile.getVelocity(),
                    target.getLocation(),
                    target.getVelocity()
            );
            engine.addSmoothParticle(target.getLocation(), new Vector2f(0f, 0f), 10f, 1f, 1f, Color.WHITE);
            engine.addSmoothParticle(new Vector2f(target.getLocation().x + target.getVelocity().x, target.getLocation().y + target.getVelocity().y), new Vector2f(0f, 0f), 20f, 1f, 1f, Color.RED);
            engine.addSmoothParticle(lead, new Vector2f(0f, 0f), 50f, 1f, 1f, Color.MAGENTA);
            engine.addSmoothParticle(new Vector2f(missile.getLocation().x + missile.getVelocity().x, missile.getLocation().y + missile.getVelocity().y), new Vector2f(0f, 0f), 20f, 1f, 1f, Color.BLUE);*/
            //null pointer protection
            if (lead == null ) {
                lead = target.getLocation();
            }
        }

        //best velocity vector angle for interception
        float correctAngle = VectorUtils.getAngle(missile.getLocation(), lead);

        //velocity angle correction
        float offCourseAngle = MathUtils.getShortestRotation(
                VectorUtils.getFacing(missile.getVelocity()),
                correctAngle
        );

        float correction = MathUtils.getShortestRotation(
                correctAngle,
                VectorUtils.getFacing(missile.getVelocity())+180
        )
                * 0.5f * //oversteer
                (float)((FastTrig.sin(MathUtils.FPI/90*(Math.min(Math.abs(offCourseAngle),45))))); //damping when the correction isn't important

        //modified optimal facing to correct the velocity vector angle as soon as possible
        correctAngle = correctAngle+correction;

        //turn the missile
        float aimAngle = MathUtils.getShortestRotation(missile.getFacing(), correctAngle);
        if (aimAngle < 0) {
            missile.giveCommand(ShipCommand.TURN_RIGHT);
        } else {
            missile.giveCommand(ShipCommand.TURN_LEFT);
        }
        if (Math.abs(aimAngle)<45){
            missile.giveCommand(ShipCommand.ACCELERATE);
        }

        // Damp angular velocity if we're getting close to the target angle
        if (Math.abs(aimAngle) < Math.abs(missile.getAngularVelocity()) * DAMPING) {
            missile.setAngularVelocity(aimAngle / DAMPING);
        }
    }

    private CombatEntityAPI findRandomMissileWithinRange(MissileAPI missile){

        MissileAPI theTarget;
        ShipAPI source = missile.getSource();
        WeightedRandomPicker<MissileAPI> targets = new WeightedRandomPicker<>();

        List <MissileAPI> TARGETTED = getAntimissiles();

        for (MissileAPI m : AIUtils.getNearbyEnemyMissiles(source, 1000)){

            float danger=2f;

            //determine the proximity danger:
            if(MathUtils.isWithinRange(source, m, 333)){
                danger=4;
            } else if(MathUtils.isWithinRange(source, m, 666)){
                danger=3;
            }

            //adjust for the damage danger
            if(m.getDamageAmount()>700){
                danger++;
            } else if(m.getDamageAmount()<150){
                danger--;
            }

            //reduce the danger from missiles already under interception
            if(TARGETTED.contains(m)){
                if(m.getHitpoints()*m.getHullLevel()<=missile.getDamageAmount()){
                    danger-=2;
                } else {
                    danger--;
                }
            }

            targets.add(m, danger);
        }

        theTarget=targets.pick();
        if(theTarget!=null){
            ANTIMISSILES.put(missile, theTarget);

            theTarget.setShineBrightness(0f);
        }
        return theTarget;
    }

    void proximityFuse(){
        //damage the target
        engine.applyDamage(
                target,
                target.getLocation(),
                missile.getDamageAmount(),
                DamageType.FRAGMENTATION,
                0f,
                false,
                false,
                missile.getSource()
        );

        //damage nearby targets
        List<MissileAPI> closeMissiles = AIUtils.getNearbyEnemyMissiles(missile, 100);
        for (MissileAPI cm : closeMissiles)
        {
            if (cm!=target){
                engine.applyDamage(
                        cm,
                        cm.getLocation(),
                        (2*missile.getDamageAmount()/3) - (missile.getDamageAmount()/3) * ((float)Math.cos(3000 / (MathUtils.getDistanceSquared(missile.getLocation(), target.getLocation())+1000))+1),
                        DamageType.FRAGMENTATION,
                        0,
                        false,
                        true,
                        missile.getSource()
                );
            }
        }

        if(MagicRender.screenCheck(0.5f, missile.getLocation())){
            engine.addHitParticle(
                    missile.getLocation(),
                    new Vector2f(),
                    100,
                    1,
                    0.25f,
                    EXPLOSION_COLOR
            );

            for (int i=0; i<NUM_PARTICLES; i++){
                float axis = (float)Math.random()*360;
                float range = (float)Math.random()*100;
                engine.addHitParticle(
                        MathUtils.getPoint(missile.getLocation(), range/5, axis),
                        MathUtils.getPoint(new Vector2f(), range, axis),
                        2+(float)Math.random()*2,
                        1,
                        1+(float)Math.random(),
                        PARTICLE_COLOR
                );
            }
        }

        //kill the missile
        engine.applyDamage(
                missile,
                missile.getLocation(),
                missile.getHitpoints() * 2f,
                DamageType.FRAGMENTATION,
                0f,
                false,
                false,
                missile
        );
    }

    @Override
    public CombatEntityAPI getTarget() {
        return target;
    }

    @Override
    public void setTarget(CombatEntityAPI target) {
        this.target = target;
    }
    public void init(CombatEngineAPI engine) {
    }

    public static List<MissileAPI> getAntimissiles (){
        List<MissileAPI> missiles= new ArrayList<>();

        for (MissileAPI m : ANTIMISSILES.keySet()) {
            if(ANTIMISSILES.get(m)!=null){
                missiles.add(ANTIMISSILES.get(m));
            }
        }
        return missiles;
    }

    /**
     * Returns the best place to aim to hit a target, given its current location
     * and velocity. This method does not take acceleration into account.
     *
     * @param missileLoc     The current location of the missile.
     * @param missileVel     The current velocity of the missile.
     * @param targetLoc      The location of the target.
     * @param targetVel      The current velocity of the target.
     *
     * @return The best point to aim towards to hit {@code target} given current
     *         velocities, or {@code null} if a collision is not possible.
     *
     * @author Dark.Revenant (original by broofa @ stackoverflow.com)
     * @since 1.9
     */
    private Vector2f getAPNPoint(Vector2f missileLoc, Vector2f missileVel, Vector2f targetLoc, Vector2f targetVel) {
        // Commanded Acceleration = N * Vc * LOS_Rate  + ( N * Nt ) / 2
        Vector2f diff = new Vector2f(targetLoc.x - missileLoc.x, targetLoc.y - missileLoc.y);

        Vector2f normalNew = normaliseVector(diff);
        Vector2f normalOld = normaliseVector(oldDiff);

        if (normalNew == null || normalOld == null) {
            return null;
        }

        // LOS Rate
        Vector2f LOS = new Vector2f(normalNew.x - normalOld.x, normalNew.y - normalOld.y);
        float rate = LOS.length(); // diff.length();

        // Closing Velocity
        float vcAux = Vector2f.dot(diff, new Vector2f(targetVel.x - missileVel.x, targetVel.y - missileVel.y));
        float vc = vcAux / diff.length();

        // Nt
        float tVelDotLOS = Vector2f.dot(normalNew, targetVel);
        Vector2f scalar = new Vector2f (normalNew.x * tVelDotLOS, normalNew.y * tVelDotLOS);
        Vector2f cleanVel = new Vector2f (targetVel.x - scalar.x, targetVel.y - scalar.y);
        float vel = cleanVel.length();
//        Vector2f ntAux = VectorUtils.rotate(new Vector2f(targetVel.x - oldVel.x, targetVel.y - oldVel.y), VectorUtils.getFacing(oldVel) + 90f);
        float nt = (vel - oldVel) / engine.getElapsedInLastFrame();

        // Acceleration needed
        float a = N * vc * rate; //  + ( N * nt ) / 2;
        float b = N * vc * rate + ( N * nt ) / 2;

        // Target point
        Vector2f temp = new Vector2f(0f, b);
        Vector2f targetAux = VectorUtils.rotate(temp, VectorUtils.getFacing(diff));
        //targetAux = VectorUtils.rotate(targetAux, 90f);
        Vector2f target = new Vector2f(missileLoc.x + missileVel.x + targetAux.x, missileLoc.y + missileVel.y + targetAux.y);

        oldDiff = diff;
        oldVel = vel;
        return target;
    }

    private Vector2f normaliseVector(Vector2f vector) {
        double n = vector.length();

        if (n > 0) {
            return new Vector2f((float)(vector.y/n), (float)(vector.y/n));
        }

        return null;
    }
}
