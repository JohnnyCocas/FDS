package data.scripts.campaign.ids;

/**
 *
 * @author MadHSSniper a.k.a. Johnny Cocks a.k.a. joaonunes
 */
public class FDS_IDs {

    //Mod Factions
    public static final String FACTION_FDS = "fringe_defence_syndicate";

    //Mod Planets
    public static final String PLANET_TUNDRA = "fds_tundra";
    public static final String PLANET_GAS_GIANT = "fds_gas_giant";
    public static final String PLANET_DESERT = "fds_desert";
    public static final String PLANET_LAVA = "fds_lava";
    public static final String PLANET_CRYOVOLCANIC = "fds_cryovolcanic";
    public static final String PLANET_FROZEN = "fds_frozen";

    //Mod Stations
    public static final String STATION_GOLAN = "fds_station_golan_i";
    public static final String STATION_GOLAN_II = "fds_station_golan_ii";
    public static final String STATION_GOLAN_III = "fds_station_golan_iii";
    public static final String STATION_DEATH_STAR = "fds_station_death_star";

    //Mod Settings
    public static final String SETTINGS_STORY = "fdsStoryline";
    public static final String SETTINGS_DROIDS = "droidMechanics";

    //Mod Sector Map Labels
    public static final String LABEL_FRINGE = "fds_fringe_label";

    //Mod Buffs
    public static final String BUFF_MAINTENANCE_BOTS = "fds_maintenanceBotsBonus";
    public static final String BUFF_SINGULARITY_GENERATOR = "fds_quantum_generator";
    public static final String BUFF_OVERCLOCKED_SENSORS = "fds_overclocked_sensors";
    //Mod Debuffs
    public static final String DEBUFF_SIGNATURE_DAMPENER = "fds_signature_dampener";

    //Mod Missiles
    public static final String FDS_BARRAGE_MISSILE = "fds_barrage_missile";
    public static final String FDS_PD_MISSILE = "fds_pd_missile";
    public static final String FDS_SENSOR_MINE = "fds_sensor_mine";
}
