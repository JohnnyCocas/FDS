package data.scripts.campaign.econ;

import com.fs.starfarer.api.campaign.econ.Industry;
import com.fs.starfarer.api.impl.campaign.econ.BaseMarketConditionPlugin;
import com.fs.starfarer.api.impl.campaign.econ.impl.BaseIndustry;
import com.fs.starfarer.api.impl.campaign.ids.Industries;
import data.scripts.campaign.ids.FDS_Commodities;

/**
 *
 * @author MadHSSniper a.k.a. Johnny Cocks a.k.a. joaonunes
 */
public class FDS_CrystalCaves extends BaseMarketConditionPlugin {

	public void apply(String id) {
		super.apply(id);

		int size = market.getSize();

		Industry industry = market.getIndustry(Industries.MINING);
		if (industry == null) return;

		if (industry.isFunctional()) {
			industry.supply(id, FDS_Commodities.ENERGY_CRYSTALS, size - 2, BaseIndustry.BASE_VALUE_TEXT);
		} else {
			industry.getSupply(FDS_Commodities.ENERGY_CRYSTALS).getQuantity().unmodifyFlat(id);
		}
	}

	public void unapply(String id) {
		super.unapply(id);
	}
}
