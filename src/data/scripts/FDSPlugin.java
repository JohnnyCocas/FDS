package data.scripts;

import com.fs.starfarer.api.BaseModPlugin;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.PluginPick;
import com.fs.starfarer.api.campaign.CampaignPlugin;
import com.fs.starfarer.api.campaign.econ.EconomyAPI;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.combat.MissileAIPlugin;
import com.fs.starfarer.api.combat.MissileAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.impl.campaign.procgen.CategoryGenDataSpec;
import com.fs.starfarer.api.impl.campaign.procgen.ConditionGenDataSpec;
import com.thoughtworks.xstream.XStream;
import data.scripts.campaign.FDS_MaintenanceBotsBonus;
import data.scripts.campaign.ids.FDS_Conditions;
import data.scripts.campaign.ids.FDS_IDs;
import data.scripts.campaign.ids.FDS_Industries;
import data.scripts.weapons.ai.FDS_CustomMissileAI;
import data.scripts.weapons.ai.FDS_PDMissileAI;
import data.scripts.world.FDSGen;
import exerelin.campaign.SectorManager;
import java.io.IOException;
import java.util.List;
import java.util.Random;
import org.apache.log4j.Level;
import org.dark.shaders.light.LightData;
import org.json.JSONException;
import org.json.JSONObject;
import org.dark.shaders.util.ShaderLib;

import static data.scripts.campaign.ids.FDS_IDs.FDS_BARRAGE_MISSILE;
import static data.scripts.campaign.ids.FDS_IDs.FDS_PD_MISSILE;

/**
 *
 * @author MadHSSniper a.k.a. Johnny Cocks a.k.a. joaonunes
 */
public class FDSPlugin extends BaseModPlugin {

    private static final String SETTINGS_FILE = "FDS_OPTIONS.ini";
    public static boolean fdsStoryline = false;
    public static boolean droidMechanics = false;

    private static void loadSettings() throws IOException, JSONException {
        JSONObject settings = Global.getSettings().loadJSON(SETTINGS_FILE);

        fdsStoryline = false;//settings.getBoolean(FDS_IDs.SETTINGS_STORY);
        droidMechanics = settings.getBoolean(FDS_IDs.SETTINGS_DROIDS);
    }
    private static void loadDefaultSettings() {
        fdsStoryline = false;
        droidMechanics = false;
    }

    @Override
    public void configureXStream(XStream x) {
        x.alias("FDS_MaintenanceBotsBonus", FDS_MaintenanceBotsBonus.class);
    }

    @Override
    public void onApplicationLoad() throws ClassNotFoundException {

        try {  //LazyLib
            Global.getSettings().getScriptClassLoader().loadClass("org.lazywizard.lazylib.ModUtils");
        } catch (ClassNotFoundException ex) {
            String message = System.lineSeparator()
                    + System.lineSeparator() + "LazyLib is required to run at least one of the mods you have installed."
                    + System.lineSeparator() + System.lineSeparator()
                    + "You can download LazyLib at http://fractalsoftworks.com/forum/index.php?topic=5444"
                    + System.lineSeparator();
            throw new ClassNotFoundException(message);
        }

        try {  //MagicLib
            Global.getSettings().getScriptClassLoader().loadClass("data.scripts.plugins.MagicTrailPlugin");
        } catch (ClassNotFoundException ex) {
            String message = System.lineSeparator()
                    + System.lineSeparator() + "MagicLib is required to run FDS."
                    + System.lineSeparator() + System.lineSeparator()
                    + "You can download MagicLib at http://fractalsoftworks.com/forum/index.php?topic=13718"
                    + System.lineSeparator();
            throw new ClassNotFoundException(message);
        }

        try {
            loadSettings();
        } catch (IOException | JSONException e) {
            loadDefaultSettings();
            Global.getLogger(FDSPlugin.class).log(Level.ERROR, "Settings loading failed, using default values");
            //Global.getLogger(FDSPlugin.class).log(Level.DEBUG, "CAUSE: "+e.getCause()+"\nMESSAGE: "+e.getMessage()+"\nSTACK: "+e.getStackTrace());
        } catch (RuntimeException e){
            loadDefaultSettings();
            Global.getLogger(FDSPlugin.class).log(Level.WARN, "Settings file not found, using default values");
            //Global.getLogger(FDSPlugin.class).log(Level.DEBUG, "CAUSE: "+e.getCause()+"\nMESSAGE: "+e.getMessage()+"\nSTACK: "+e.getStackTrace());
        } catch (Exception e){
            loadDefaultSettings();
            Global.getLogger(FDSPlugin.class).log(Level.ERROR, "Settings file failed to load due to unknown reasons, using default values");
            //Global.getLogger(FDSPlugin.class).log(Level.DEBUG, "CAUSE: "+e.getCause()+"\nMESSAGE: "+e.getMessage()+"\nSTACK: "+e.getStackTrace());
        }

        try {  //GraphicsLib
            Global.getSettings().getScriptClassLoader().loadClass("org.dark.shaders.util.ShaderLib");
            ShaderLib.init();
            LightData.readLightDataCSV("data/lights/FDS_Light.csv");
//            TextureData.readTextureDataCSV("data/lights/FDS_Texture.csv");
        } catch (ClassNotFoundException ex) {
            String message = System.lineSeparator()
                    + System.lineSeparator() + "GraphicsLib is required to run at least one of the mods you have installed."
                    + System.lineSeparator() + System.lineSeparator()
                    + "You can download GraphicsLib at http://fractalsoftworks.com/forum/index.php?topic=10982"
                    + System.lineSeparator();
            throw new ClassNotFoundException(message);
        }

        generateCustomCategories();
    }

    @Override
    public void onNewGame() {
        boolean hasNexerelin = Global.getSettings().getModManager().isModEnabled("nexerelin");
        if (hasNexerelin && !SectorManager.getCorvusMode()) {
            return;
        }
        new FDSGen().generate(Global.getSector());
    }

    @Override
    public void onGameLoad(boolean newGame) {
        if (droidMechanics) {
            Global.getSector().addTransientScript(new FDS_MaintenanceBotsBonus());
        }
    }

//    @Override
//    public void onNewGameAfterEconomyLoad() {
//        if (factoryShare) {
//            if (randomFactoryShare) {
//                generateFactoriesRandom();
//            } else {
//                generateFactories();
//            }
//        }
//    }

    @Override
    public PluginPick<MissileAIPlugin> pickMissileAI(MissileAPI missile, ShipAPI launchingShip)
    {
        switch (missile.getProjectileSpecId()) {
            case FDS_BARRAGE_MISSILE:
                return new PluginPick(new FDS_CustomMissileAI(missile, launchingShip), CampaignPlugin.PickPriority.MOD_SET);
            case FDS_PD_MISSILE:
                return new PluginPick(new FDS_PDMissileAI(missile, launchingShip), CampaignPlugin.PickPriority.MOD_SET);

        }

        return null;
    }

    public void generateCustomCategories() {
        for (Object o : Global.getSettings().getAllSpecs(ConditionGenDataSpec.class)) {
            ConditionGenDataSpec spec = (ConditionGenDataSpec) o;
            // CONDITIONS
            if (spec.getId().equals("extreme_tectonic_activity")) {
                spec.getMultipliers().put("fds_lava", 100f);
                spec.getMultipliers().put("fds_cryovolcanic", 1f);
            }
            if (spec.getId().equals("tectonic_activity")) {
                spec.getMultipliers().put("fds_cryovolcanic", 10f);
            }
            if (spec.getId().equals("very_cold")) {
                spec.getMultipliers().put("fds_cryovolcanic", 1f);
            }
            if (spec.getId().equals("very_hot")) {
                spec.getMultipliers().put("fds_lava", 100f);
            }
            if (spec.getId().equals("atmosphere_no_pick")) {
                spec.getMultipliers().put("fds_cryovolcanic", 15f);
            }
            if (spec.getId().equals("thin_atmosphere")) {
                spec.getMultipliers().put("fds_cryovolcanic", 1f);
            }
            if (spec.getId().equals("toxic_atmosphere")) {
                spec.getMultipliers().put("fds_lava", 100f);
                spec.getMultipliers().put("fds_cryovolcanic", 1f);
            }
            if (spec.getId().equals("dense_atmosphere")) {
                spec.getMultipliers().put("fds_lava", 100f);
                spec.getMultipliers().put("fds_cryovolcanic", 1f);
            }
            if (spec.getId().equals("biosphere_no_pick")) {
                spec.getMultipliers().put("fds_lava", 100f);
                spec.getMultipliers().put("fds_cryovolcanic", 100f);
            }
            if (spec.getId().equals("inimical_biosphere")) {
                spec.getMultipliers().put("fds_lava", 1f);
                spec.getMultipliers().put("fds_cryovolcanic", 1f);
            }
            if (spec.getId().equals("ruins_no_pick")) {
                spec.getMultipliers().put("fds_lava", 1000f);
                spec.getMultipliers().put("fds_cryovolcanic", 200f);
            }
            if (spec.getId().equals("ruins_scattered")) {
                spec.getMultipliers().put("fds_lava", 3f);
                spec.getMultipliers().put("fds_cryovolcanic", 15f);
            }
            if (spec.getId().equals("ruins_widespread")) {
                spec.getMultipliers().put("fds_lava", 1f);
                spec.getMultipliers().put("fds_cryovolcanic", 10f);
            }
            if (spec.getId().equals("ruins_extensive")) {
                spec.getMultipliers().put("fds_cryovolcanic", 5f);
            }
            if (spec.getId().equals("ruins_vast")) {
                spec.getMultipliers().put("fds_cryovolcanic", 1f);
            }
            if (spec.getId().equals("decivilized_no_pick")) {
                spec.getMultipliers().put("fds_lava", 100f);
                spec.getMultipliers().put("fds_cryovolcanic", 100f);
            }
            if (spec.getId().equals("decivilized")) {
                spec.getMultipliers().put("fds_lava", 10f);
                spec.getMultipliers().put("fds_cryovolcanic", 1f);
            }
            if (spec.getId().equals("weather_no_pick")) {
                spec.getMultipliers().put("fds_cryovolcanic", 10f);
            }
            if (spec.getId().equals("extreme_weather")) {
                spec.getMultipliers().put("fds_cryovolcanic", 1f);
            }
            if (spec.getId().equals("pollution_no_pick")) {
                spec.getMultipliers().put("fds_cryovolcanic", 15f);
            }
            if (spec.getId().equals("pollution")) {
                spec.getMultipliers().put("fds_cryovolcanic", 1f);
            }

            // ORE
            if (spec.getId().equals("ore_no_pick")) {
                spec.getMultipliers().put("fds_lava", 0f);
                spec.getMultipliers().put("fds_cryovolcanic", 0f);
            }
            if (spec.getId().equals("ore_sparse")) {
                spec.getMultipliers().put("fds_lava", 0f);
                spec.getMultipliers().put("fds_cryovolcanic", 4f);
            }
            if (spec.getId().equals("ore_moderate")) {
                spec.getMultipliers().put("fds_lava", 10f);
                spec.getMultipliers().put("fds_cryovolcanic", 20f);
            }
            if (spec.getId().equals("ore_abundant")) {
                spec.getMultipliers().put("fds_lava", 30f);
                spec.getMultipliers().put("fds_cryovolcanic", 10f);
            }
            if (spec.getId().equals("ore_rich")) {
                spec.getMultipliers().put("fds_lava", 30f);
                spec.getMultipliers().put("fds_cryovolcanic", 3f);
            }
            if (spec.getId().equals("ore_ultrarich")) {
                spec.getMultipliers().put("fds_lava", 20f);
                spec.getMultipliers().put("fds_cryovolcanic", 1f);
            }

            // RARE ORE
            if (spec.getId().equals("rare_ore_no_pick")) {
                spec.getMultipliers().put("fds_lava", 5f);
                spec.getMultipliers().put("fds_cryovolcanic", 20f);
            }
            if (spec.getId().equals("rare_ore_sparse")) {
                spec.getMultipliers().put("fds_lava", 10f);
                spec.getMultipliers().put("fds_cryovolcanic", 10f);
            }
            if (spec.getId().equals("rare_ore_moderate")) {
                spec.getMultipliers().put("fds_lava", 10f);
                spec.getMultipliers().put("fds_cryovolcanic", 10f);
            }
            if (spec.getId().equals("rare_ore_abundant")) {
                spec.getMultipliers().put("fds_lava", 30f);
                spec.getMultipliers().put("fds_cryovolcanic", 10f);
            }
            if (spec.getId().equals("rare_ore_rich")) {
                spec.getMultipliers().put("fds_lava", 25f);
                spec.getMultipliers().put("fds_cryovolcanic", 5f);
            }
            if (spec.getId().equals("rare_ore_ultrarich")) {
                spec.getMultipliers().put("fds_lava", 15f);
                spec.getMultipliers().put("fds_cryovolcanic", 3f);
            }

            // VOLATILES
            if (spec.getId().equals("volatiles_trace")) {
                spec.getMultipliers().put("fds_cryovolcanic", 10f);
            }
            if (spec.getId().equals("volatiles_diffuse")) {
                spec.getMultipliers().put("fds_cryovolcanic", 15f);
            }
            if (spec.getId().equals("volatiles_abundant")) {
                spec.getMultipliers().put("fds_cryovolcanic", 15f);
            }
            if (spec.getId().equals("volatiles_plentiful")) {
                spec.getMultipliers().put("fds_cryovolcanic", 5f);
            }

            // SPECIAL
            if (spec.getId().equals("fds_crystal_caves_no_pick")) {
                spec.getMultipliers().put("fds_cryovolcanic", 50f);
            }
            if (spec.getId().equals("fds_crystal_caves")) {
                spec.getMultipliers().put("fds_cryovolcanic", 1f);
            }
        }
    }
}
