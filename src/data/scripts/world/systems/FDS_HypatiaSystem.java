package data.scripts.world.systems;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.*;
import com.fs.starfarer.api.impl.campaign.ids.*;
import com.fs.starfarer.api.impl.campaign.procgen.NebulaEditor;
import com.fs.starfarer.api.impl.campaign.procgen.StarAge;
import com.fs.starfarer.api.impl.campaign.terrain.HyperspaceTerrainPlugin;
import com.fs.starfarer.api.impl.campaign.DerelictShipEntityPlugin.DerelictShipData;
import com.fs.starfarer.api.impl.campaign.procgen.themes.BaseThemeGenerator;
import com.fs.starfarer.api.impl.campaign.procgen.themes.SalvageSpecialAssigner.ShipRecoverySpecialCreator;
import com.fs.starfarer.api.impl.campaign.rulecmd.salvage.special.ShipRecoverySpecial.PerShipData;
import com.fs.starfarer.api.impl.campaign.rulecmd.salvage.special.ShipRecoverySpecial.ShipCondition;
import com.fs.starfarer.api.util.Misc;
import data.scripts.campaign.ids.FDS_Conditions;
import data.scripts.campaign.ids.FDS_IDs;
import data.scripts.campaign.ids.FDS_Industries;
import data.scripts.campaign.ids.Vanilla_PlanetTypes;
import data.scripts.world.FDS_AddMarket;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author MadHSSniper a.k.a. Johnny Cocks a.k.a. joaonunes
 */
public class FDS_HypatiaSystem implements SectorGeneratorPlugin {

    @Override
    public void generate(SectorAPI sector) {

        StarSystemAPI system = sector.createStarSystem("Hypatia");
        system.setBackgroundTextureFilename("graphics/backgrounds/background4.jpg");

        PlanetAPI star = system.initStar("hypatiaStar", StarTypes.ORANGE, 400f, 200);
        star.setName("Hypatia");
        system.setLightColor(new Color(250, 255, 250));

        system.getLocation().set(7000f, 8000f);

        SectorEntityToken hypatiaNebula = Misc.addNebulaFromPNG("data/campaign/terrain/fds_hypatia_nebula.png",
                0, 0,
                system,
                "terrain", "nebula",
                4, 4, StarAge.AVERAGE);

        PlanetAPI inferna = system.addPlanet("infernaPlanet", star, "Inferna", Vanilla_PlanetTypes.VOLCANIC, (float)Math.random()*360, 100, 2200, 100);
        Misc.initConditionMarket(inferna);
        inferna.getMarket().addCondition(Conditions.VERY_HOT);
        inferna.getMarket().addCondition(Conditions.TECTONIC_ACTIVITY);
        inferna.getMarket().addCondition(Conditions.ORE_ULTRARICH);

        PlanetAPI vulnere = system.addPlanet("vulnerePlanet", star, "Vulnere", Vanilla_PlanetTypes.DESERT, (float)Math.random()*360, 150, 2900, 170);
        Misc.initConditionMarket(vulnere);
        vulnere.getMarket().addCondition(Conditions.ARID);
        vulnere.getMarket().addCondition(Conditions.RARE_ORE_MODERATE);
        vulnere.getMarket().addCondition(Conditions.FARMLAND_POOR);
        vulnere.getMarket().addCondition(Conditions.DESERT);
        vulnere.getMarket().addCondition(Conditions.HOT);

        system.addAsteroidBelt(star, 90, 3650, 400, 50, 200, Terrain.ASTEROID_BELT,  "Fury's Belt");
        system.addRingBand(star, "misc", "rings_dust0", 256f, 3, Color.white, 256f, 3600, 205f, null, null);
        system.addRingBand(star, "misc", "rings_asteroids0", 256f, 3, Color.white, 256f, 3720, 195f, null, null);

        PlanetAPI ira = system.addPlanet("iraPlanet", star, "Ira", Vanilla_PlanetTypes.GAS_GIANT, (float)Math.random()*360, 500, 5100, 225);
        system.addRingBand(ira, "misc", "rings_ice0", 256f, 2, Color.white, 256f, 1200, 45, Terrain.RING, null);

        PlanetAPI ultor = system.addPlanet("ultorPlanet", ira, "Ultor", FDS_IDs.PLANET_TUNDRA, (float)Math.random()*360, 120, 800, 30);
        FDS_AddMarket.FDS_AddMarket(FDS_IDs.FACTION_FDS,
                ultor,
                null,//new ArrayList<>(Arrays.asList(Global.getSector().getStarSystem("Hypatia").getEntityById("arbitriumPlanet"))),
                "Ultor", // Market
                4,
                new ArrayList<>(Arrays.asList(
                        Conditions.HABITABLE,
                        Conditions.POPULATION_3,
                        Conditions.ORE_MODERATE,
                        Conditions.ORGANICS_TRACE)),
                new ArrayList<>(Arrays.asList(
                        Industries.GROUNDDEFENSES,
                        Industries.SPACEPORT,
                        Industries.MINING,
                        Industries.REFINING,
                        Industries.LIGHTINDUSTRY,
                        Industries.HEAVYINDUSTRY,
                        Industries.MILITARYBASE,
                        Industries.POPULATION)),
                new ArrayList<>(Arrays.asList(
                        Submarkets.SUBMARKET_BLACK,
                        Submarkets.SUBMARKET_OPEN,
                        Submarkets.GENERIC_MILITARY)),
                0.35f
        );

        SectorEntityToken hypatiaRelay = system.addCustomEntity("hypatiaRelay", // unique id
                "Hypatia Relay", // name - if null, defaultName from custom_entities.json will be used
                "comm_relay", // type of object, defined in custom_entities.json
                FDS_IDs.FACTION_FDS); // faction
        hypatiaRelay.setCircularOrbitPointingDown(star, 240, 7600, 310);

        PlanetAPI arbitrium = system.addPlanet("arbitriumPlanet", star, "Arbitrium", FDS_IDs.PLANET_FROZEN, (float)Math.random()*360, 116, 9600, 400);
        arbitrium.setFaction(FDS_IDs.FACTION_FDS);
        FDS_AddMarket.FDS_AddMarket(FDS_IDs.FACTION_FDS,
                arbitrium,
                null,//new ArrayList<>(Arrays.asList(Global.getSector().getStarSystem("Hypatia").getEntityById("arbitriumPlanet"))),
                "Arbitrium", // Market
                3,
                new ArrayList<>(Arrays.asList(
                        Conditions.VOLATILES_ABUNDANT,
                        Conditions.COLD,
                        Conditions.POPULATION_1,
                        Conditions.RARE_ORE_ULTRARICH,
                        Conditions.ICE,
                        FDS_Conditions.CRYSTAL_CAVES)),
                new ArrayList<>(Arrays.asList(
                        Industries.GROUNDDEFENSES,
                        Industries.PATROLHQ,
                        Industries.SPACEPORT,
                        Industries.MINING,
                        Industries.LIGHTINDUSTRY,
                        Industries.POPULATION,
                        FDS_Industries.FUEL_CONVERTER)),
                new ArrayList<>(Arrays.asList(
                        Submarkets.SUBMARKET_BLACK,
                        Submarkets.SUBMARKET_STORAGE,
                        Submarkets.SUBMARKET_OPEN,
                        Submarkets.GENERIC_MILITARY)),
                0.35f
        );

        JumpPointAPI arbitriumJumpPoint = Global.getFactory().createJumpPoint("arbitriumJumpPoint", "Arbitrium Jump-Point");
        OrbitAPI orbit = Global.getFactory().createCircularOrbit(arbitrium, arbitrium.getCircularOrbitAngle()-45, 9000, 400);
        arbitriumJumpPoint.setOrbit(orbit);
        arbitriumJumpPoint.setRelatedPlanet(arbitrium);
        arbitriumJumpPoint.setStandardWormholeToHyperspaceVisual();
        system.addEntity(arbitriumJumpPoint);

        system.autogenerateHyperspaceJumpPoints(true, true, true);

        cleanup(system);
    }

    void cleanup(StarSystemAPI system) {
        HyperspaceTerrainPlugin plugin = (HyperspaceTerrainPlugin) Misc.getHyperspaceTerrain().getPlugin();
        NebulaEditor editor = new NebulaEditor(plugin);
        float minRadius = plugin.getTileSize() * 2f;

        float radius = system.getMaxRadiusInHyperspace();
        editor.clearArc(system.getLocation().x, system.getLocation().y, 0, radius + minRadius * 0.5f, 0, 360f);
        editor.clearArc(system.getLocation().x, system.getLocation().y, 0, radius + minRadius, 0, 360f, 0.25f);
    }

    protected void addDerelict(StarSystemAPI system, SectorEntityToken focus, String variantId,
                               ShipCondition condition, float orbitRadius, boolean recoverable) {
        DerelictShipData params = new DerelictShipData(new PerShipData(variantId, condition), false);
        SectorEntityToken ship = BaseThemeGenerator.addSalvageEntity(system, Entities.WRECK, Factions.NEUTRAL, params);
        ship.setDiscoverable(true);

        float orbitDays = orbitRadius / (10f + (float) Math.random() * 5f);
        ship.setCircularOrbit(focus, (float) Math.random() * 360f, orbitRadius, orbitDays);

        if (recoverable) {
            ShipRecoverySpecialCreator creator = new ShipRecoverySpecialCreator(null, 0, 0, false, null, null);
            Misc.setSalvageSpecial(ship, creator.createSpecial(ship, null));
        }

    }
}
