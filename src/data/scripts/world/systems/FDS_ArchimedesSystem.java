package data.scripts.world.systems;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.JumpPointAPI;
import com.fs.starfarer.api.campaign.OrbitAPI;
import com.fs.starfarer.api.campaign.PlanetAPI;
import com.fs.starfarer.api.campaign.SectorAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.SectorGeneratorPlugin;
import com.fs.starfarer.api.campaign.StarSystemAPI;
import com.fs.starfarer.api.impl.campaign.ids.*;
import com.fs.starfarer.api.impl.campaign.procgen.NebulaEditor;
import com.fs.starfarer.api.impl.campaign.procgen.StarAge;
import com.fs.starfarer.api.impl.campaign.DerelictShipEntityPlugin.DerelictShipData;
import com.fs.starfarer.api.impl.campaign.procgen.themes.BaseThemeGenerator;
import com.fs.starfarer.api.impl.campaign.procgen.themes.SalvageSpecialAssigner.ShipRecoverySpecialCreator;
import com.fs.starfarer.api.impl.campaign.rulecmd.salvage.special.ShipRecoverySpecial.PerShipData;
import com.fs.starfarer.api.impl.campaign.rulecmd.salvage.special.ShipRecoverySpecial.ShipCondition;
import com.fs.starfarer.api.impl.campaign.terrain.HyperspaceTerrainPlugin;
import com.fs.starfarer.api.util.Misc;
import data.scripts.campaign.ids.FDS_Industries;
import data.scripts.world.FDS_AddMarket;
import data.scripts.campaign.ids.Vanilla_PlanetTypes;
import data.scripts.campaign.ids.FDS_IDs;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author MadHSSniper a.k.a. Johnny Cocks a.k.a. joaonunes
 */
public class FDS_ArchimedesSystem implements SectorGeneratorPlugin {

    @Override
    public void generate(SectorAPI sector) {

        StarSystemAPI system = sector.createStarSystem("Archimedes");
        system.setBackgroundTextureFilename("graphics/backgrounds/background4.jpg");

        PlanetAPI star = system.initStar("archimedesStar",
                StarTypes.BLUE_GIANT,
                900f,
                700, // extent of corona outside star
                10f, // solar wind burn level
                1f, // flare probability
                4f); // CR loss multiplier, good values are in the range of 1-5
        star.setName("Archimedes");
        system.setLightColor(new Color(240, 240, 255));

        system.getLocation().set(12000f, 7000f);

        SectorEntityToken archimedesNebula = Misc.addNebulaFromPNG("data/campaign/terrain/fds_archimedes_nebula.png",
                0, 0,
                system,
                "terrain", "nebula_amber",
                4, 4, StarAge.OLD);

        PlanetAPI exitium = system.addPlanet("exitiumPlanet", star, "Exitium", FDS_IDs.PLANET_LAVA, 0, 60, 2300, 60);
        Misc.initConditionMarket(exitium);
        exitium.getMarket().addCondition(Conditions.TECTONIC_ACTIVITY);
        exitium.getMarket().addCondition(Conditions.DENSE_ATMOSPHERE);
        exitium.getMarket().addCondition(Conditions.VERY_HOT);
        exitium.getMarket().addCondition(Conditions.ORE_RICH);
        exitium.getMarket().addCondition(Conditions.RARE_ORE_SPARSE);

        PlanetAPI desolatio = system.addPlanet("desolatioPlanet", star, "Desolatio", FDS_IDs.PLANET_DESERT, 90, 50, 3200, 130);
        Misc.initConditionMarket(desolatio);
        desolatio.getMarket().addCondition(Conditions.ARID);
        desolatio.getMarket().addCondition(Conditions.VOLATILES_TRACE);
        desolatio.getMarket().addCondition(Conditions.FARMLAND_POOR);
        desolatio.getMarket().addCondition(Conditions.DESERT);

        PlanetAPI caries = system.addPlanet("cariesPlanet", star, "Caries", FDS_IDs.PLANET_CRYOVOLCANIC, 270, 65, 4000, 200);
        Misc.initConditionMarket(caries);
        caries.getMarket().addCondition(Conditions.IRRADIATED);
        caries.getMarket().addCondition(Conditions.TOXIC_ATMOSPHERE);
        caries.getMarket().addCondition(Conditions.RARE_ORE_ABUNDANT);
        caries.getMarket().addCondition(Conditions.ORGANICS_COMMON);

        system.addAsteroidBelt(star, 100, 5075, 400, 75, 125, Terrain.ASTEROID_BELT,  "Edison's Belt");
        system.addRingBand(star, "misc", "rings_dust0", 256f, 3, Color.white, 256f, 5000, 190f, null, null);
        system.addRingBand(star, "misc", "rings_asteroids0", 256f, 3, Color.white, 256f, 5100, 210f, null, null);

        PlanetAPI novaeSpes = system.addPlanet("spesPlanet", star, "Novae Spes", Vanilla_PlanetTypes.TERRAN, 25, 122, 5800, 250);
        novaeSpes.setFaction(FDS_IDs.FACTION_FDS);

        SectorEntityToken archimedes_relay = system.addCustomEntity("archimedesRelay", "Archimedes Relay", "comm_relay", FDS_IDs.FACTION_FDS);
        archimedes_relay.setCircularOrbitPointingDown(star, 120, 6750, 300);

        SectorEntityToken newtonStation = system.addCustomEntity("newtonStation", "Newton Space Station", "station_side00", FDS_IDs.FACTION_FDS);
        newtonStation.setCircularOrbitPointingDown(novaeSpes, 0, 260, 100);

        FDS_AddMarket.FDS_AddMarket(FDS_IDs.FACTION_FDS,
                novaeSpes,
                new ArrayList<>(Arrays.asList((SectorEntityToken) newtonStation)),
                "Novae Spes",
                7,
                new ArrayList<>(Arrays.asList(
                        Conditions.MILD_CLIMATE,
                        Conditions.FARMLAND_BOUNTIFUL,
                        Conditions.HABITABLE,
                        Conditions.ORGANICS_COMMON,
//                        Conditions.MILITARY_BASE,
//                        Conditions.SPACEPORT,
//                        Conditions.TRADE_CENTER,
//                        Conditions.HEADQUARTERS,
                        Conditions.REGIONAL_CAPITAL,
                        Conditions.POPULATION_8,
//                        Conditions.HYDROPONICS_COMPLEX,
//                        Conditions.ORBITAL_STATION,
//                        Conditions.ORBITAL_BURNS,
                        Conditions.URBANIZED_POLITY)),
                new ArrayList<>(Arrays.asList(
                        Industries.HIGHCOMMAND,
                        Industries.LIGHTINDUSTRY,
                        Industries.ORBITALWORKS,
                        Industries.MEGAPORT,
//                        Industries.WAYSTATION,
                        Industries.STARFORTRESS_MID,
                        Industries.FARMING,
                        Industries.MINING,
                        Industries.FUELPROD,
//                        Industries.REFINING,
                        Industries.HEAVYBATTERIES,
                        Industries.POPULATION,
                        FDS_Industries.REPAIR_YARDS,
                        FDS_Industries.MAINTENANCE_BOTS_FACTORY)),
                new ArrayList<>(Arrays.asList(
                        Submarkets.SUBMARKET_STORAGE,
                        Submarkets.SUBMARKET_BLACK,
                        Submarkets.SUBMARKET_OPEN,
                        Submarkets.GENERIC_MILITARY)),
                0.35f
        );

        JumpPointAPI spesJumpPoint = Global.getFactory().createJumpPoint("spesJumpPoint", "Spes Jump-Point");
        OrbitAPI orbit = Global.getFactory().createCircularOrbit(star, 42, 6200, 250);
        spesJumpPoint.setOrbit(orbit);
        spesJumpPoint.setRelatedPlanet(novaeSpes);
        spesJumpPoint.setStandardWormholeToHyperspaceVisual();
        system.addEntity(spesJumpPoint);

        SectorEntityToken gate = system.addCustomEntity("archimedesGate", // unique id
                "Archimedes Gate", // name - if null, defaultName from custom_entities.json will be used
                "inactive_gate", // type of object, defined in custom_entities.json
                null); // faction
        gate.setCircularOrbit(star, 0, 7000, 310);

        PlanetAPI tribulatio = system.addPlanet("tribulatioPlanet", star, "Tribulatio", FDS_IDs.PLANET_GAS_GIANT, (float)Math.random()*360, 460, 9100, 225);
        system.addRingBand(tribulatio, "misc", "rings_ice0", 256f, 2, Color.white, 200f, 1000, 50, Terrain.RING, null);
        PlanetAPI luctus = system.addPlanet("luctusPlanet", tribulatio, "Luctus", FDS_IDs.PLANET_TUNDRA, 120, 50, 700, 42);
        Misc.initConditionMarket(luctus);
        luctus.getMarket().addCondition(Conditions.COLD);
        luctus.getMarket().addCondition(Conditions.THIN_ATMOSPHERE);
        luctus.getMarket().addCondition(Conditions.ORE_RICH);

        system.addAsteroidBelt(star, 250, 12000, 500, 500, 600, Terrain.ASTEROID_BELT,  "Aristotle's Belt");
        system.addRingBand(star, "misc", "rings_asteroids0", 256f, 0, Color.white, 256f, 12000, 550f);

        PlanetAPI tristitia = system.addPlanet("tristitiaPlanet", star, "Tristitia", FDS_IDs.PLANET_FROZEN, 12, 60, 15000, 600);
        Misc.initConditionMarket(tristitia);
        tristitia.getMarket().addCondition(Conditions.VERY_COLD);
        tristitia.getMarket().addCondition(Conditions.NO_ATMOSPHERE);
        tristitia.getMarket().addCondition(Conditions.VOLATILES_PLENTIFUL);
        tristitia.getMarket().addCondition(Conditions.DARK);

//        StarSystemGenerator.addOrbitingEntities(system, star, StarAge.AVERAGE,
//                2, 4, // min/max entities to add
//                12500, // radius to start adding at
//                3, // name offset - next planet will be <system name> <roman numeral of this parameter + 1>
//                true); // whether to use custom or system-name based names
//
//        StarSystemGenerator.addSystemwideNebula(system, StarAge.OLD);

        system.autogenerateHyperspaceJumpPoints(true, true, true);

        cleanup(system);
    }

    void cleanup(StarSystemAPI system) {
        HyperspaceTerrainPlugin plugin = (HyperspaceTerrainPlugin) Misc.getHyperspaceTerrain().getPlugin();
        NebulaEditor editor = new NebulaEditor(plugin);
        float minRadius = plugin.getTileSize() * 2f;

        float radius = system.getMaxRadiusInHyperspace();
        editor.clearArc(system.getLocation().x, system.getLocation().y, 0, radius + minRadius * 0.5f, 0, 360f);
        editor.clearArc(system.getLocation().x, system.getLocation().y, 0, radius + minRadius, 0, 360f, 0.25f);
    }

    protected void addDerelict(StarSystemAPI system, SectorEntityToken focus, String variantId,
                               ShipCondition condition, float orbitRadius, boolean recoverable) {
        DerelictShipData params = new DerelictShipData(new PerShipData(variantId, condition), false);
        SectorEntityToken ship = BaseThemeGenerator.addSalvageEntity(system, Entities.WRECK, Factions.NEUTRAL, params);
        ship.setDiscoverable(true);

        float orbitDays = orbitRadius / (10f + (float) Math.random() * 5f);
        ship.setCircularOrbit(focus, (float) Math.random() * 360f, orbitRadius, orbitDays);

        if (recoverable) {
            ShipRecoverySpecialCreator creator = new ShipRecoverySpecialCreator(null, 0, 0, false, null, null);
            Misc.setSalvageSpecial(ship, creator.createSpecial(ship, null));
        }

    }
}
