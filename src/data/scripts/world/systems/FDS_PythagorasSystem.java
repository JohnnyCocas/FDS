package data.scripts.world.systems;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.*;
import com.fs.starfarer.api.campaign.events.CampaignEventManagerAPI;
import com.fs.starfarer.api.campaign.events.CampaignEventTarget;
import com.fs.starfarer.api.impl.campaign.ids.*;
import com.fs.starfarer.api.impl.campaign.procgen.NebulaEditor;
import com.fs.starfarer.api.impl.campaign.procgen.StarAge;
import com.fs.starfarer.api.impl.campaign.procgen.StarSystemGenerator;
import com.fs.starfarer.api.impl.campaign.terrain.HyperspaceTerrainPlugin;
import com.fs.starfarer.api.impl.campaign.DerelictShipEntityPlugin.DerelictShipData;
import com.fs.starfarer.api.impl.campaign.procgen.themes.BaseThemeGenerator;
import com.fs.starfarer.api.impl.campaign.procgen.themes.SalvageSpecialAssigner.ShipRecoverySpecialCreator;
import com.fs.starfarer.api.impl.campaign.rulecmd.salvage.special.ShipRecoverySpecial.PerShipData;
import com.fs.starfarer.api.impl.campaign.rulecmd.salvage.special.ShipRecoverySpecial.ShipCondition;
import com.fs.starfarer.api.util.Misc;
import data.scripts.FDSPlugin;
import data.scripts.campaign.FDS_Storyline;
import data.scripts.campaign.ids.FDS_IDs;
import data.scripts.world.FDS_AddMarket;
import data.scripts.campaign.ids.Vanilla_PlanetTypes;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author MadHSSniper a.k.a. Johnny Cocks a.k.a. joaonunes
 */
public class FDS_PythagorasSystem implements SectorGeneratorPlugin {

    @Override
    public void generate(SectorAPI sector) {

        StarSystemAPI system = sector.createStarSystem("Pythagoras");
        system.setBackgroundTextureFilename("graphics/backgrounds/background4.jpg");

        system.setType(StarSystemGenerator.StarSystemType.BINARY_CLOSE);
        PlanetAPI pythagoras1 = system.initStar("pythagoras1Star",
                StarTypes.YELLOW,
                780f,
                500, // extent of corona outside star
                6f, // solar wind burn level
                1f, // flare probability
                1f); // CR loss multiplier, good values are in the range of 1-5
        pythagoras1.setName("Pythagoras I");

        PlanetAPI pythagoras2 = system.addPlanet("pythagoras2Star", pythagoras1, "Pythagoras II", StarTypes.ORANGE, 10, 500, 2000, 1000);
        system.setSecondary(pythagoras2);
        system.addCorona(pythagoras2, 300, 6f, 0f, 1f);
        system.setLightColor(new Color(255, 250, 250));

        system.getLocation().set(10000f, 12000f);

        PlanetAPI odium = system.addPlanet("odiumPlanet", pythagoras1, "Odium", FDS_IDs.PLANET_DESERT, 25, 104, 4000, 100);
        odium.setFaction(Factions.INDEPENDENT);
        FDS_AddMarket.FDS_AddMarket(Factions.INDEPENDENT,
                odium,
                null,
                "Odium",
                3,
                new ArrayList<>(Arrays.asList(
                        Conditions.FARMLAND_POOR,
                        Conditions.RARE_ORE_MODERATE,
//                        Conditions.SPACEPORT,
                        Conditions.VOLATILES_PLENTIFUL,
                        Conditions.POPULATION_4,
                        Conditions.VICE_DEMAND,
                        Conditions.HOT,
                        Conditions.ORGANIZED_CRIME,
                        Conditions.ARID)),
                new ArrayList<>(Arrays.asList(
                        Industries.REFINING,
                        Industries.SPACEPORT,
                        Industries.FARMING,
                        Industries.FUELPROD,
                        Industries.MINING,
                        Industries.POPULATION
                )),
                new ArrayList<>(Arrays.asList(
                        Submarkets.SUBMARKET_STORAGE,
                        Submarkets.SUBMARKET_BLACK,
                        Submarkets.SUBMARKET_OPEN)),
                0.25f
        );

        
        if (FDSPlugin.fdsStoryline) {
            FDS_Storyline event = new FDS_Storyline();
            //CampaignEventPlugin event = (CampaignEventPlugin) new FDS_Storyline();

            CampaignEventManagerAPI eventManager = Global.getSector().getEventManager();
            CampaignEventTarget target = new CampaignEventTarget(odium);
            target.setExtra(Misc.genUID());

            event = (FDS_Storyline) eventManager.primeEvent(target, "FDS_Storyline", this);
            eventManager.startEvent(event);
        }
        
        
        JumpPointAPI odiumJumpPoint = Global.getFactory().createJumpPoint("odiumJumpPoint", "Odium Jump-Point");
        OrbitAPI orbit = Global.getFactory().createCircularOrbit(pythagoras1, 70, 4000, 100);
        odiumJumpPoint.setOrbit(orbit);
        odiumJumpPoint.setRelatedPlanet(odium);
        odiumJumpPoint.setStandardWormholeToHyperspaceVisual();
        system.addEntity(odiumJumpPoint);

        PlanetAPI timor = system.addPlanet("timorPlanet", pythagoras1, "Timor", Vanilla_PlanetTypes.GAS_GIANT, 130, 280, 5500, 150);

        SectorEntityToken timorStation = system.addCustomEntity("timorStation", "Timor Pirate Base", "station_pirate_type", Factions.PIRATES);
        timorStation.setCircularOrbitWithSpin(timor, 135, 350f, 250f, 3f, 5f);
        timorStation.setInteractionImage("illustrations", "orbital");
        FDS_AddMarket.FDS_AddMarket(Factions.PIRATES,
                timorStation,
                null,
                "Timor Pirate Base",
                4,
                new ArrayList<>(Arrays.asList(
                        Conditions.POPULATION_4,
                        Conditions.FREE_PORT,
                        Conditions.VICE_DEMAND,
                        Conditions.ORGANIZED_CRIME)),
                new ArrayList<>(Arrays.asList(
                        Industries.ORBITALSTATION,
                        Industries.SPACEPORT,
                        Industries.MILITARYBASE,
                        Industries.POPULATION)),
                new ArrayList<>(Arrays.asList(
                        Submarkets.SUBMARKET_BLACK,
                        Submarkets.SUBMARKET_OPEN,
                        Submarkets.SUBMARKET_STORAGE)),
                0.3f
        );

        PlanetAPI patiens = system.addPlanet("patiensPlanet", pythagoras1, "Patiens", Vanilla_PlanetTypes.ICE_GIANT, 290, 200, 8200, 210);

        StarSystemGenerator.addSystemwideNebula(system, StarAge.OLD);

        system.autogenerateHyperspaceJumpPoints(true, true, true);

        cleanup(system);
    }

    void cleanup(StarSystemAPI system) {
        HyperspaceTerrainPlugin plugin = (HyperspaceTerrainPlugin) Misc.getHyperspaceTerrain().getPlugin();
        NebulaEditor editor = new NebulaEditor(plugin);
        float minRadius = plugin.getTileSize() * 2f;

        float radius = system.getMaxRadiusInHyperspace();
        editor.clearArc(system.getLocation().x, system.getLocation().y, 0, radius + minRadius * 0.5f, 0, 360f);
        editor.clearArc(system.getLocation().x, system.getLocation().y, 0, radius + minRadius, 0, 360f, 0.25f);
    }

    protected void addDerelict(StarSystemAPI system, SectorEntityToken focus, String variantId,
                               ShipCondition condition, float orbitRadius, boolean recoverable) {
        DerelictShipData params = new DerelictShipData(new PerShipData(variantId, condition), false);
        SectorEntityToken ship = BaseThemeGenerator.addSalvageEntity(system, Entities.WRECK, Factions.NEUTRAL, params);
        ship.setDiscoverable(true);

        float orbitDays = orbitRadius / (10f + (float) Math.random() * 5f);
        ship.setCircularOrbit(focus, (float) Math.random() * 360f, orbitRadius, orbitDays);

        if (recoverable) {
            ShipRecoverySpecialCreator creator = new ShipRecoverySpecialCreator(null, 0, 0, false, null, null);
            Misc.setSalvageSpecial(ship, creator.createSpecial(ship, null));
        }

    }
}
