package data.scripts.plugins;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.BaseEveryFrameCombatPlugin;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.util.IntervalUtil;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lwjgl.util.vector.Vector2f;
import java.util.*;

/**
 *
 * @author MadHSSniper a.k.a. Johnny Cocks a.k.a. joaonunes
 */
public class FDS_QuantumSignatureCalculator extends BaseEveryFrameCombatPlugin {

    private CombatEngineAPI engine;
    private final IntervalUtil tracker = new IntervalUtil(2f, 4f);

    public static List<ThreatLevels> shipDangerLevel = new ArrayList<>();
    public static List<JumpIntentions> jumpIntentions = new ArrayList<>();
    private static List<ShipAPI> jumpers = new ArrayList<>();

    private static Map weight = new HashMap();
    static {
        weight.put(ShipAPI.HullSize.FIGHTER, 0f);
        weight.put(ShipAPI.HullSize.FRIGATE, 1f);
        weight.put(ShipAPI.HullSize.DESTROYER, 4f);
        weight.put(ShipAPI.HullSize.CRUISER, 7f);
        weight.put(ShipAPI.HullSize.CAPITAL_SHIP, 10f);
    }

    private static Map personality = new HashMap();
    static {
        personality.put("timid", 0.5f);
        personality.put("cautious", 0.75f);
        personality.put("steady", 1f);
        personality.put("aggressive", 1.2f);
        personality.put("reckless", 1.5f);
    }

    @Override
    public void init(CombatEngineAPI engine) {
        this.engine = engine;
    }

    @Override
    public void advance(float amount, List events) {
        if (this.engine != Global.getCombatEngine()) {
            this.engine = Global.getCombatEngine();
        }
        if (this.engine != null) {
            if (this.engine.isPaused()) {
                return;
            }
            this.tracker.advance(amount);
            if (this.tracker.intervalElapsed()) {
                List<ShipAPI> ships = CombatUtils.getShipsWithinRange(new Vector2f(engine.getMapWidth() / 2, engine.getMapHeight() / 2), 100000f);
                Iterator i$ = ships.iterator();

                ShipAPI ship;
                while(i$.hasNext()) {
                    ship = (ShipAPI)i$.next();
                    bringOutYourDead();
                    if (!ship.getHullSpec().getHullSize().equals(ShipAPI.HullSize.FIGHTER) && !ship.isHulk()) {
                        calculateShipDangerLevels(ship);
                    }
                }
                generateJumpPoints();
            }
        }
    }

    public static void addJumper(ShipAPI ship) {
        jumpers.add(ship);
    }

    private void bringOutYourDead() {
        if (shipDangerLevel != null && shipDangerLevel.size() > 0) {
            Iterator i$ = shipDangerLevel.iterator();
            ThreatLevels t;
            while(i$.hasNext()) {
                t = (ThreatLevels) i$.next();
                if (t.isDead()) {
                    i$.remove();
                }
            }
        }

        if (jumpers != null && jumpers.size() > 0) {
            Iterator s$ = jumpers.iterator();
            ShipAPI s;
            while(s$.hasNext()) {
                s = (ShipAPI) s$.next();
                if (s == null || s.isHulk()) {
                    s$.remove();
                }
            }
        }

        if (jumpIntentions != null && jumpIntentions.size() > 0) {
            Iterator j$ = jumpIntentions.iterator();
            JumpIntentions j;
            while(j$.hasNext()) {
                j = (JumpIntentions) j$.next();
                if (j.isShipDead()) {
                    j$.remove();
                }
            }
        }
    }



    private void calculateShipDangerLevels(ShipAPI ship) {
        ThreatLevels threatLevels = new ThreatLevels();
        threatLevels.setShip(ship);

        float threat = (float)weight.get(ship.getHullSpec().getHullSize());
        if (ship.getVariant().hasTag("STATION")) { threat *= 10; }

        List<ShipAPI> enemyShips = AIUtils.getEnemiesOnMap(ship);
        threatLevels.setEnemy(calculateThreat(ship, enemyShips));

        List<ShipAPI> alliedShips = AIUtils.getAlliesOnMap(ship);
        threatLevels.setAllied(threat + calculateThreat(ship, alliedShips));

        boolean check = false;
        if (shipDangerLevel != null) {
            for (ThreatLevels t : shipDangerLevel) {
                if (t.getShip() == ship) {
                    int idx = shipDangerLevel.indexOf(t);
                    if (idx >= 0) {
                        check = true;
                        shipDangerLevel.set(idx, threatLevels);
                    }
                }
            }
            if (!check) {
                shipDangerLevel.add(threatLevels);
            }
        } else {
            shipDangerLevel.add(threatLevels);
        }
    }

    private float calculateThreat(ShipAPI ship, List<ShipAPI> ships) {
        float threat = 0f;
        float temp;

        Iterator i$ = ships.iterator();
        ShipAPI s;
        while(i$.hasNext()) {
            s = (ShipAPI)i$.next();
            if (!s.getHullSpec().getHullSize().equals(ShipAPI.HullSize.FIGHTER) && !s.isHulk()) {
                temp = (float)weight.get(s.getHullSpec().getHullSize());
                if (s.getVariant().hasTag("STATION")) { temp *= 10; }
                threat += (float)((temp / MathUtils.getDistance(ship, s)) * 1000f);
            }
        }
        return threat;
    }

    private float calculateThreat(Vector2f point, List<ShipAPI> ships) {
        float threat = 0f;
        float temp;

        Iterator i$ = ships.iterator();
        ShipAPI s;
        while(i$.hasNext()) {
            s = (ShipAPI)i$.next();
            if (!s.getHullSpec().getHullSize().equals(ShipAPI.HullSize.FIGHTER) && !s.isHulk()) {
                temp = (float)weight.get(s.getHullSpec().getHullSize());
                if (s.getVariant().hasTag("STATION")) { temp *= 10; }
                threat += (float)((temp / MathUtils.getDistance(point, s.getLocation())) * 1000f);
            }
        }
        return threat;
    }

    private void generateJumpPoints() {
        if (shipDangerLevel != null && shipDangerLevel.size() > 0) {
            Iterator i$ = shipDangerLevel.iterator();
            ThreatLevels t;
            while(i$.hasNext()) {
                t = (ThreatLevels) i$.next();

                List<ShipAPI> enemyShips = AIUtils.getEnemiesOnMap(t.getShip());

                List<ShipAPI> alliedShips = AIUtils.getAlliesOnMap(t.getShip());

                List<PointThreat> points = t.getPointThreat();
                Iterator p$ = points.iterator();
                PointThreat p;
                float angle = 0f;
                float shipAngle;
                float baseDistance = 800f;
                float clearCount = 0f;
                Vector2f loc = t.getShip().getLocation();
                while(p$.hasNext()) {
                    p = (PointThreat) p$.next();

                    shipAngle = t.getShip().getFacing();
                    Vector2f vector = new Vector2f(baseDistance * (float)Math.cos(shipAngle + angle), baseDistance * (float)Math.sin(shipAngle + angle));
                    Vector2f point = new Vector2f(loc.getX() + vector.getX(), loc.getY() + vector.getY());
                    p.setPoint(point);

                    p.setAllied(calculateThreat(point, alliedShips));
                    p.setEnemy(calculateThreat(point, enemyShips));

                    if (CombatUtils.getShipsWithinRange(point, 300f).size() > 0) {
                        p.setClear(false);
                    } else {
                        clearCount++;
                        p.setClear(true);
                    }
                    angle += 45f;
                }
                t.setClearPoints(clearCount);
            }
        }
    }

    public class JumpIntentions {
        private ShipAPI ship;
        private PointThreat jumpPoint;
        private boolean maneuvering = false;
        private boolean ready = false;
        private boolean canJump = false;

        public JumpIntentions() {
        }

        public JumpIntentions(PointThreat jumpPoint) {
            setJumpPoints(jumpPoint);
        }

        public JumpIntentions(ShipAPI ship, PointThreat jumpPoint) {
            setShip(ship);
            setJumpPoints(jumpPoint);
        }

        public ShipAPI getShip() {
            return ship;
        }

        private PointThreat getJumpPoint() {
            return jumpPoint;
        }

        public boolean isManeuvering() {
            return maneuvering;
        }

        public boolean isReady() {
            return canJump && ready;
        }

        public boolean canJump() {
            return canJump;
        }

        public void setReady(boolean ready) {
            this.ready = ready;
        }

        public void setCanJump(boolean canJump) {
            this.canJump = canJump;
        }

        private void setJumpPoints(PointThreat jumpPoint) {
            this.jumpPoint = jumpPoint;
        }

        public void setManeuvering(boolean maneuvering) {
            this.maneuvering = maneuvering;
        }

        public void setShip(ShipAPI ship) {
            this.ship = ship;
        }

        public boolean isShipDead() {
            return this.ship != null ? this.ship.isHulk() : true;
        }
    }

    private class PointThreat {
        private Vector2f point;
        private float allied;
        private float enemy;
        private boolean clear;

        public PointThreat() {
            setAllied(0f);
            setEnemy(0f);
            setClear(true);
        }

        public void setPoint(Vector2f point) {
            this.point = point;
        }

        public void setAllied(float allied) {
            this.allied = allied;
        }

        public void setEnemy(float enemy) {
            this.enemy = enemy;
        }

        public void setClear(boolean clear) {
            this.clear = clear;
        }

        public Vector2f getPoint() {
            return point;
        }

        public float getAllied() {
            return allied;
        }

        public float getEnemy() {
            return enemy;
        }

        public boolean isClear() {
            return clear;
        }
    }

    public class ThreatLevels {
        private ShipAPI ship;
        private float allied;
        private float enemy;
        private List<PointThreat> pointThreat;
        private float clearPoints;

        public ThreatLevels() {
            setAllied(0f);
            setEnemy(0f);
            setClearPoints(0f);
            pointThreat = new ArrayList<>();
            for (int i = 0; i < 8; i++) {
                pointThreat.add(new PointThreat());
            }
        }

        public void setShip(ShipAPI ship) {
            this.ship = ship;
        }

        public void setAllied(float allied) {
            this.allied = allied;
        }

        public void setEnemy(float enemy) {
            this.enemy = enemy;
        }

        public void setClearPoints(float clearPoints) {
            this.clearPoints = clearPoints;
        }

        public void setPointThreat(List<PointThreat> pointThreat) {
            this.pointThreat = pointThreat;
        }

        public float getAllied() {
            return allied;
        }

        public float getEnemy() {
            return enemy;
        }

        public List<PointThreat> getPointThreat() {
            return pointThreat;
        }
        public float numClearPoints() {
            return clearPoints;
        }

        public boolean isDead() {
            return this.ship != null ? this.ship.isHulk() : true;
        }

        public ShipAPI getShip() {
            return ship;
        }
    }

    public static JumpIntentions getJumpIntentionsShip(ShipAPI ship) {
        if (ship == null) { return null; }

        Iterator i$ = jumpIntentions.iterator();
        JumpIntentions t;
        while(i$.hasNext()) {
            t = (JumpIntentions) i$.next();
            if (t.getShip() == ship) {
                return t;
            }
        }
        return null;
    }

    public static ThreatLevels getShipDangerLevels(ShipAPI ship) {
        Iterator i$ = shipDangerLevel.iterator();
        ThreatLevels t;
        while(i$.hasNext()) {
            t = (ThreatLevels) i$.next();
            if (t.getShip() == ship) {
                return t;
            }
        }
        return null;
    }
}
