package data.scripts.plugins;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.BaseEveryFrameCombatPlugin;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.MissileAPI;
import com.fs.starfarer.api.combat.ShipAPI;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import com.fs.starfarer.api.combat.ShipHullSpecAPI.ShipTypeHints;
import com.fs.starfarer.api.util.IntervalUtil;
import data.scripts.campaign.ids.FDS_IDs;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lwjgl.util.vector.Vector2f;

import static data.hullmods.FDS_AugmentedSensorArrays.RANGE_BONUS;
import static data.scripts.campaign.ids.FDS_IDs.FDS_SENSOR_MINE;

/**
 *
 * @author MadHSSniper a.k.a. Johnny Cocks a.k.a. joaonunes
 */
public class FDS_FactionCombatBuff extends BaseEveryFrameCombatPlugin {

    public static final Object KEY_STATUS = new Object();
    private CombatEngineAPI engine;
    private ShipAPI playerShip;
    private final String BUFF_ID = "fds_syndicates_might";
    public static final float MAX_RANGE = 3000f;
    public static final float SENSOR_MINE_RANGE = 1000f;
    public static final float MAX_BOOST = 0.5f;
    public static final float ALLIED_BOOST = 0.5f;
    private final IntervalUtil tracker = new IntervalUtil(1f, 1f);
    private final IntervalUtil fasterTracker = new IntervalUtil(0.1f, 0.1f);

    private List<ShipAPI> sensors = new ArrayList();

    @Override
    public void init(CombatEngineAPI engine) {
        this.engine = engine;
    }

    @Override
    public void advance(float amount, List events) {
        if (this.engine != Global.getCombatEngine()) {
            this.engine = Global.getCombatEngine();
        }
        if (this.playerShip == null || this.playerShip != engine.getPlayerShip()) {
            this.playerShip = engine.getPlayerShip();
        }
        if (this.engine != null) {
            this.processSensorMod();
            this.processSensorMines();

            if (this.engine.isPaused()) {
                return;
            }
            this.tracker.advance(amount);
            this.fasterTracker.advance(amount);

            if (this.tracker.intervalElapsed()) {
                List<ShipAPI> ships = CombatUtils.getShipsWithinRange(new Vector2f(engine.getMapWidth() / 2, engine.getMapHeight() / 2), 100000f);
                Iterator i$ = ships.iterator();

                ShipAPI ship;
                while(i$.hasNext()) {
                    ship = (ShipAPI)i$.next();
                    processShip(ship);
                }
            }

            // Generate status icon
            if (this.playerShip != null && this.playerShip.getMutableStats().getCRLossPerSecondPercent().getMultBonus(BUFF_ID) != null && this.playerShip.getVariant().hasHullMod("fds_syndicate_combat_systems")) {
                String icon = Global.getSettings().getSpriteName("ui", "fds_combat_buff");
                String title = "Nearby allied ships: " + countPlayerNearbyShips();
                String data = "-" + (int) ((1 - this.playerShip.getMutableStats().getCRLossPerSecondPercent().getMultBonus(BUFF_ID).value) * 100) + "% CR loss";

                engine.maintainStatusForPlayerShip(KEY_STATUS, icon, title, data, false);
            }
        }
    }

    private void processShip(ShipAPI s) {
        List<ShipAPI> ships = AIUtils.getAlliesOnMap(s);
        Iterator i$ = ships.iterator();

        ShipAPI ship;
        while(i$.hasNext()) {
            ship = (ShipAPI)i$.next();
            if (ship.getVariant().hasHullMod("fds_syndicate_combat_systems")) {
                // ship.getMutableStats().getCRLossPerSecondPercent().unmodify(BUFF_ID);
                ship.getMutableStats().getCRLossPerSecondPercent().modifyMult(BUFF_ID, calculateBonus(ship));
                // ship.getMutableStats().getPeakCRDuration().modifyPercent(BUFF_ID, (calculateBonus(ship) * 100)); // BROKEN!
            }
        }
    }

    private void processSensorMines() {
        List<MissileAPI> mines = engine.getMissiles();
        Iterator i$ = mines.iterator();

        MissileAPI sensor;
        while(i$.hasNext()) {
            sensor = (MissileAPI)i$.next();
            if (sensor.getProjectileSpecId() == FDS_SENSOR_MINE) {
                engine.getFogOfWar(sensor.getOwner()).revealAroundPoint(sensor, sensor.getLocation().x, sensor.getLocation().y, SENSOR_MINE_RANGE);
            }
        }
    }

    private void processSensorMod() {
        List<ShipAPI> ships = engine.getShips();
        Iterator i$ = ships.iterator();

        ShipAPI ship;
        while(i$.hasNext()) {
            ship = (ShipAPI)i$.next();
            if (ship.getVariant().hasHullMod("fds_augmented_sensor_arrays")) {
                boolean hasBuff = ship.getMutableStats().getSensorStrength().getPercentStatMod(FDS_IDs.BUFF_OVERCLOCKED_SENSORS) != null;
                if (hasBuff) {
                    Vector2f loc = ship.getLocation();
                    boolean ally = ship.isAlly() || ship.getOwner() == 0;
                    engine.getFogOfWar(ally ? 0 : 1).revealAroundPoint(ship, loc.x, loc.y, MAX_RANGE + MAX_RANGE * (RANGE_BONUS / 100));
                }
            }
        }
    }

    private float calculateBonus(ShipAPI ship) {
        List<ShipAPI> ships = AIUtils.getNearbyAllies(ship, MAX_RANGE);
        Iterator i$ = ships.iterator();
        ShipAPI s;
        float count = 0;
        while(i$.hasNext()) {
            s = (ShipAPI)i$.next();
            if (s.getHullSpec().getHullId().startsWith("fds_")
            && !s.getHullSpec().getHullSize().equals(ShipAPI.HullSize.FIGHTER)
            && !s.getHullSpec().getHints().contains(ShipTypeHints.CIVILIAN)) {
                count++;
            } else if (!s.getHullSpec().getHullId().startsWith("fds_")
                    && !s.getHullSpec().getHullSize().equals(ShipAPI.HullSize.FIGHTER)
                    && !s.getHullSpec().getHints().contains(ShipTypeHints.CIVILIAN)) {
                count += ALLIED_BOOST;
            }
        }
        return count / 20 > MAX_BOOST ? MAX_BOOST : 1 - (count / 20);
    }

    private int countPlayerNearbyShips() {
        List<ShipAPI> ships = AIUtils.getNearbyAllies(this.playerShip, MAX_RANGE);
        Iterator i$ = ships.iterator();
        ShipAPI s;
        int count = 0;
        while(i$.hasNext()) {
            s = (ShipAPI)i$.next();
            if (!s.getHullSpec().getHullSize().equals(ShipAPI.HullSize.FIGHTER)
            && !s.getHullSpec().getHints().contains(ShipTypeHints.CIVILIAN)) {
                count++;
            }
        }
        return count;
    }
}
