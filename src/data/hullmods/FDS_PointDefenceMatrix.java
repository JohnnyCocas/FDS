package data.hullmods;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;
import com.fs.starfarer.api.combat.WeaponAPI;
import com.fs.starfarer.api.combat.WeaponAPI.WeaponSize;
import com.fs.starfarer.api.combat.WeaponAPI.WeaponType;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 *
 * @author MadHSSniper a.k.a. Johnny Cocks a.k.a. joaonunes
 */
public class FDS_PointDefenceMatrix extends BaseHullMod {

//    public static final float RANGE_DEBUFF = -10f;       //10% decreaae

    private static final Set<String> BLOCKED_HULLMODS = new HashSet<>(1);
    private String ERROR = "FDSIncompatibleHullmodWarning";

    static {
        BLOCKED_HULLMODS.add("pointdefenseai");
    }

    public void applyEffectsAfterShipCreation(ShipAPI ship, String id) {
        for (String tmp : BLOCKED_HULLMODS) {
            if (ship.getVariant().getHullMods().contains(tmp)) {
                ship.getVariant().removeMod(tmp);
                ship.getVariant().addMod(ERROR);
            }
        }

        List weapons = ship.getAllWeapons();
        Iterator iter = weapons.iterator();
        while (iter.hasNext()) {
            WeaponAPI weapon = (WeaponAPI)iter.next();
            if ((weapon.getSize() == WeaponSize.SMALL || weapon.getSize() == WeaponSize.MEDIUM) && weapon.getType() != WeaponType.MISSILE) {
                weapon.setPD(true);
            }
        }
    }

    @Override
    public String getUnapplicableReason(ShipAPI ship) {
        if (!(ship.getHullSpec().getHullId().startsWith("fds_atonement"))) {
            return "Can only be installed on the Atonement-class Cruiser";
        }
        if (ship.getVariant().getHullMods().contains("pointdefenseai")) {
            return "Incompatible with Integrated Point Defense AI";
        }

        return null;
    }

    @Override
    public boolean isApplicableToShip(ShipAPI ship) {
        // Allows any ship with a fds hull id
        return !ship.getVariant().getHullMods().contains("pointdefenseai") && ship.getHullSpec().getHullId().startsWith("fds_atonement");
    }

//    public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id) {
//        stats.getBeamWeaponRangeBonus().modifyPercent(id, RANGE_DEBUFF);
//    }

    public String getDescriptionParam(int index, HullSize hullSize) {
        return null;
    }
}