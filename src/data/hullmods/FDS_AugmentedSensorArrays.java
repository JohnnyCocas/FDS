package data.hullmods;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;

import java.util.*;

public class FDS_AugmentedSensorArrays extends BaseHullMod {

    public static final float RANGE_BONUS = 20f; //20% increase
    private static final Set<String> BLOCKED_HULLMODS = new HashSet<>(1);
    private String ERROR = "FDSIncompatibleHullmodWarning";

    static {
        BLOCKED_HULLMODS.add("hiressensors");
    }

    @Override
    public void applyEffectsAfterShipCreation(ShipAPI ship, String id) {
        for (String tmp : BLOCKED_HULLMODS) {
            if (ship.getVariant().getHullMods().contains(tmp)) {
                ship.getVariant().removeMod(tmp);
                ship.getVariant().addMod(ERROR);
            }
        }
    }

    @Override
    public String getUnapplicableReason(ShipAPI ship) {
        if (!(ship.getHullSpec().getHullId().startsWith("fds_"))) {
            return "Must be installed on a FDS ship";
        }
        if (ship.getVariant().getHullMods().contains("hiressensors")) {
            return "Incompatible with High Resolution Sensors";
        }

        return null;
    }

    @Override
    public boolean isApplicableToShip(ShipAPI ship) {
        // Allows any ship with a fds hull id
        return ship.getHullSpec().getHullId().startsWith("fds_") &&
                !ship.getVariant().getHullMods().contains("hiressensors");
    }

    public void applyEffectsBeforeShipCreation(ShipAPI.HullSize hullSize, MutableShipStatsAPI stats, String id) {
        stats.getSensorStrength().modifyPercent(id, RANGE_BONUS);
    }

    public String getDescriptionParam(int index, ShipAPI.HullSize hullSize) {
        if (index == 0) {
            return "" + (int) RANGE_BONUS;
        }
        return null;
    }
}