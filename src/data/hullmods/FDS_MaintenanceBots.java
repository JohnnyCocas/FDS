package data.hullmods;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;

/**
 *
 * @author MadHSSniper a.k.a. Johnny Cocks a.k.a. joaonunes
 */
@SuppressWarnings("unchecked")
public class FDS_MaintenanceBots extends BaseHullMod {

    public static final float REPAIR_BONUS = 0.75f;       //25% increase
    public static final float DISABLED_BONUS = 0.5f;       //50% reduction

    public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id) {
        stats.getCombatEngineRepairTimeMult().modifyMult(id, REPAIR_BONUS);
        stats.getCombatWeaponRepairTimeMult().modifyMult(id, REPAIR_BONUS);
        stats.getOverloadTimeMod().modifyMult(id, DISABLED_BONUS);
    }

    public String getDescriptionParam(int index, HullSize hullSize) {
        if (index == 0) {
            return "" + (int)((1 - REPAIR_BONUS) * 100);
        } else if (index == 1) {
            return "" + (int)((1 - DISABLED_BONUS) * 100);
        }
        return null;
    }
}
